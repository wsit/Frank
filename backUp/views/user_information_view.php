<?php include 'header.php' ?>
 <?php include 'advertise_left.php' ?>
 <?php include 'advertise_right.php' ?>
 <script type="text/javascript">
				function is_valid_email(email){
				    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				    if (!filter.test(email)) 
				           return false;
				    else
				        return true;
				}
								
				function submit_update_basic_info()
				{
				
					if(basic_info_validation()){
						$.ajax({
						    url:$('#base_url').val()+'update/submit_update_basic_information',
							data: {f_name:$("#f_name").val(),l_name:$("#l_name").val(),gender:$("#gender").val(),month:$("#month").val(),day:$("#day").val(),year:$("#year").val(),pic_path:$('#pic_path').val()},
							type: "POST",
							success: function(data){
								var resp=data.split(";");
								if(resp[1]=="True"){

									$('#basic_info_div').hide();
									$('#basic_info_span_head').addClass("span_head").removeClass("span_head_bold");
									$('#profile_data_step').addClass("step").removeClass("step_blur");
									$('#profile_data_span_head').addClass("span_head_bold").removeClass("span_head");	
									$('#profile_data_div').show();
									
								}else if(resp[1]=="False"){
										$('#'+resp[2]).css('background','yellow');
										$('#error_div').html(resp[3]);
								}
							}
						});
					}
				}
				
				function submit_update_profile_data()
				{
					
					$.ajax({
                       url:$('#base_url').val()+'update/update_profile_data',
                       data:{current_location_1:$('#current_location_1').val(),current_location_2:$('#current_location_2').val(),home_town_1:$('#home_town_1').val(),home_town_2:$('#home_town_2').val(),organization_1:$('#organization_1').val(),organization_2:$('#organization_2').val(),high_school:$('#high_school').val(),higher_education_1:$('#higher_education_1').val(),higher_education_2:$('#higher_education_2').val(),workplace_1:$('#workplace_1').val(),workplace_2:$('#workplace_2').val()},
                       type:"POST",
                       success: function(data){
                    	   	$('#profile_data_div').hide();
                    	   	$('#preference_settings_div').show();
                    	   	$('#preference_settings_step').addClass("step").removeClass("step_blur");
							$('#profile_data_span_head').addClass("span_head").removeClass("span_head_bold");	
							$('#preference_settings_span_head').addClass("span_head_bold").removeClass("span_head");	
							
						}
					});
				}
				function skip_update_profile_data()
				{
					$('#profile_data_div').hide();
            	   	$('#preference_settings_div').show();
            	   	$('#preference_settings_step').addClass("step").removeClass("step_blur");
					$('#profile_data_span_head').addClass("span_head").removeClass("span_head_bold");	
					$('#preference_settings_span_head').addClass("span_head_bold").removeClass("span_head");	
									
								
				}
				
				function submit_update_perference_settings()
				{
					$.ajax({
					url:$('#base_url').val()+'update/update_preference_strring',
					data:{birthday_hidden:$('#birthday_hidden').val(),home_page:$('#home_page').val(),security:$('#security').val(),gossip:$('#gossip').val(),notification:$('#notification').val()},
					type:"POST",
					 success:function(data){
						 $('#preference_settings_div').hide();
						 $('#preference_settings_span_head').addClass("span_head").removeClass("span_head_bold");	
						 $('#invite_friends_span_head').addClass("span_head_bold").removeClass("span_head");	
						 $('#invite_friends_step').addClass("step").removeClass("step_blur");		
						 $('#invite_friends_div').show();
					}
					});
				}
				function skip_update_preference_strring()
				{
					$('#preference_settings_div').hide();
					$('#preference_settings_span_head').addClass("span_head").removeClass("span_head_bold");	
					$('#invite_friends_span_head').addClass("span_head_bold").removeClass("span_head");	
					$('#invite_friends_step').addClass("step").removeClass("step_blur");		
					$('#invite_friends_div').show();
				}
				
				function submit_update_invite_friends_data()
				{
					$.ajax({
                        url:'signup/update_invite_friends_data',
                        data:{friend_name:$('#friend_name').val(),friend_email:$('#friend_email').val()},
                        type:"POST",
                        success:function(data){
                        	$('#invite_friends_div').hide();
        					$('#registration_success_mail_account').html("<b>"+$('#u_email').attr("value")+"<b>");
        					$('#registration_success_mail_account').show();
    					}
						});
				}
				function skip_update_invite_friends_data()
				{
					$('#invite_friends_div').hide();
					$('#registration_success_mail_account').html("<b>"+$('#u_email').attr("value")+"<b>");
					$('#registration_success').show();
					
				}
				function is_valid_email(value){
					
					   var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					   if (!filter.test(value)) 
						return false;
					   else
						return true;
				}
				
				function check_email(value)
				{
					var email_one=$("#password").val().length;
					//var email_two=$("#r_password").val().length;
					if(email_one!=value)
					{
						alert('Email address must be same');
					}
				}
				
				function basic_info_validation()
				{
					if($('#f_name').val()==''){
						$('#f_name').css("background","Red").focus();
                                             return false;
                                     }else
                                         $('#f_name').css("background","white");
                                     
					
                                     if($('#l_name').val()==''){
						$('#l_name').css("background","Red").focus();
                                             return false;
                                     }else
                                        $('#l_name').css("background","white");
					
					if($('#gender').val()==''){
                                         $('#gender').css("background","Red").focus();
                                         return false;
                                     }else
                                         $('#gender').css("background","white");
					
					if($('#year').val()==''){
						$('#year').css('background','Red').focus();
                                             return false;
                                     }else
                                         $('#year').css("background","white");

					if($('#month').val()==''){
                        $('#month').css('background','Red').focus();
                        return false;
                    }else
                        $('#month').css("background","white");
                    
                                     
					if($('#day').val()==''){
						$('#day').css('background','Red').focus();
                                             return false;
                                     }else
                                         $('#day').css("background","white");
                                     
					
					
                                    
				
					
					return true;
                                     
				}
				</script>
				<div class="registration">
					<input type="hidden" id="base_url" value="<?php echo base_url();?>">
					<div class="top_container">
						<div class="top_info">
							<div class="top_p">
								<p style="padding:3px; line-height:17px;" >
									This is a site for everyone brave enough to put that mirror</br>
									in fornt of you and be hudge, not on your skill or your </br>
									worldview, just how you are as a frined and a member of </br>
									society. Fell Free to browse and look at anyones reputation </br>
									but remember, if you want to judge, you have to be open </br>
									to be judge too. Leave somthing good about a family </br>
									member or somthing nasty about a classmate,
								</p>
								
							</div>
						</div>
						
						<div class="top_status">
							<div class="status1">
								<div class="statePara">
									<span>25,5455,24 People</span>
								</div>
							</div>
							<div class="status2">
								<div class="statePara">
									<span>25,5455,24 People</span>
								</div>
							</div>
							<div class="status3">
								<div class="statePara">
									<span>25,5455,24 People</span>
								</div>
							</div>
						
						</div>
						
						<div class="top_best">
							<div class="heading">
							</div>
							<div class="personality">
								<img src="<?php echo base_url(); ?>images/george_clooney.png" height="100" width="70" />
									<div class="personality_info">
									<span class="plarge_span">George Clooney</span>
									<span class="psmall_span">New York &nbsp 1/15/11</span>
										<div id="szlider">
											<div id="szliderbar">
											</div>
											<div id="szazalek">
											</div>
										</div>
										<div id="szlider">
											<div id="szliderbar">
											</div>
											<div id="szazalek">
											</div>
										</div>
									</div>	
							</div>
						</div>
					</div>
					<div class="signupwrap">
						<div class="signup_head">
							<div class="span_head">
								<span id="basic_info_span_head" class="span_head_bold">Basic Info</span>
							</div>
							<div class="span_head">
								<span id="profile_data_span_head" class="span_head">Profile Data</span>
							</div>	
							<div class="span_head">
								<span id="preference_settings_span_head" class="span_head">Preference Settings</span>
							</div>
							<div  class="span_head">
								<span id="invite_friends_span_head" class="span_head">Invite Friend</span>
							</div>
						</div>
						<div class="signup_content">
							<div class="step_wrap">
								<div id="basic_info_step" class="step">
									<span>Step1:Basic Info</span>
								</div>
								<div id="profile_data_step" class="step_blur">
									<span>Step2:Profile Data</span>
								</div>
								<div id="preference_settings_step" class="step_blur">
									<span>Step3:Preference Settings</span>
								</div>
								<div id="invite_friends_step" class="step_blur">
									<span>Step4:Invite Friend</span>
								</div>
							</div>
							<div id="basic_info_div">
								<form class="reg_form">
								
									<div class="reg_content">
									<?php foreach ($basic_info_all as $rowData) {?>
										<div class="reg_field">
											<span class="reg_field_span">First name :</span>
											<input type="text" class="reg_input_large" value="<?php echo $rowData->f_name;?>" name="f_name" id="f_name">
										</div>
										<div class="reg_field">
											<span class="reg_field_span">Last Name :</span>
											<input type="text" class="reg_input_large" value="<?php echo $rowData->l_name;?>" name="l_name" id="l_name">
										</div>
										<div class="reg_field">
											<span class="reg_field_span">Gender :</span>
	                                                                                <select class="reg