<?php include 'admin_header.php' ?>
<script>
    function update_site_static_page(suffix){
        $.ajax({
                type:"post",
                url:$('#site_url').attr("value")+"site_static_page/update_site_static_page_"+suffix+"_row",
                data:{details:$('#details').val()},
                success:function(data){
                   var resp=data.split(';');
                    if(resp[1]=="True"){
                        $('#update_static_page_content_view').fadeOut(500,function(){
                            $('#update_static_page_content_view').css("background","yellow");
                            $('#update_static_page_content_view').css("color","black");
                            $('#update_static_page_content_view').css("text-align","center");
                            $('#update_static_page_content_view').html(resp[2]);
                            $('#update_static_page_content_view').fadeIn(500).delay(2000).fadeOut(500,
                              function(){
                                         $('#update_static_page_content_view').html("");
                                         $('#update_static_page_content_view').css("background","white");
                                         $('#update_static_page_content_view').css("color","white");
                                         $('#update_static_page_content_view').css("text-align","");
                                        }
                                    );
                            });
                    }else if(resp[1]=="False"){
                        $('#update_static_page_content_view').fadeOut(500,function(){
                            $('#update_static_page_content_view').css("background","yellow");
                            $('#update_static_page_content_view').html(resp[2]);
                            $('#update_static_page_content_view').fadeIn(500).delay(2000).fadeOut(500,
                                        function(){
                                                $('#update_static_page_content_view').html("");
                                        }
                                    );
                        });
                    }
                }
            });
    }
    function load_site_static_page(suffix){
        $.ajax({
                type:"post",
                url:$('#site_url').val()+"site_static_page/get_site_static_page_"+suffix,
                success:function(data){
                    
                    $('#update_static_page_content_view').hide();
                    $('#update_static_page_content_view').html(data);
                    $('#update_static_page_content_view').fadeIn(500,function(){
                        $('#details').focus();
                    });
                }
        });
    }
    
    function cancel_submit_update_mail_template_by_type(){
        $('#update_static_page_content_view').fadeOut(500,function(){
           $('#update_static_page_content_view').html(""); 
          
        });
    }
</script>
<?php include 'admin_dashboard.php' ?>

<div class="admin_panel">
	<?php include 'top_container.php' ?>
	<div class="admin_container">
		<div class="criteria">
			<div class="head_div">
				<p class="head_div_desc">Static Page contents</p>
			</div>
			<div id="signup_div" class="message_temp">
				<p>1.</p>
				<p>About Frank</p>
				<div class="btn_message_edit">
						<a href="javascript:load_site_static_page('about')"><span>Edit</span></a>
				</div>
			</div>
			<div class="message_temp">
				<p>2.</p>
				<p>FAQ</p>
				<div class="btn_message_edit">
					<a href="javascript:load_site_static_page('faq')"><span>Edit</span></a>
				</div>
			</div>
			<div class="message_temp">
				<p>3.</p>
				<p>Contact Us</p>
				<div class="btn_message_edit">
					<a href="javascript:load_site_static_page('contact')"><span>Edit</span></a>
				</div>
			</div>
			<div class="message_temp">
				<p>4.</p>
				<p>Site Map</p>
				<div class="btn_message_edit">
                                    <a href="javascript:load_site_static_page('map')"><span>Edit</span></a>
				</div>
			</div>
			<div class="message_temp">
				<p>5.</p>
				<p>Terms of Service</p>
				<div class="btn_message_edit">
                                    <a href="javascript:load_site_static_page('terms')"><span>Edit</span></a>
				</div>
			</div>
			<div class="message_temp">
				<p>6.</p>
				<p>Privacy Policy</p>
				<div class="btn_message_edit">
                                    <a href="javascript:load_site_static_page('privacy')"><span>Edit</span></a>
                                </div>
			</div>

		</div><br><br><br>

		<div id="update_static_page_content_view">
                </div>
			


	</div>
</div>	
<?php include 'footer.php' ?>
<!-- Hidden Tags [Starts]-->
<input type="hidden" id="site_url" value="<?php echo $site_url; ?>"
<!-- Hidden Tags [Ends]-->