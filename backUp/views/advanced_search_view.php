 <?php include 'header.php' ?>
<script>
function auto_suggestion(){
        $(function() {
		$("input#name" ).autocomplete({
			source: function(request, response) {
				$.ajax({ url:$('#base_url').val()+'suggestion/get_name',
				data: { term: $("#name").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);
				}
			});
		},
		minLength: 1
		});
		
	});
    	$(function() {
		$("input#high_school" ).autocomplete({
			source: function(request, response) {
				$.ajax({ url:$('#base_url').val()+'suggestion/get_high_school_suggestion_from_user_profile_data',
				data: { term: $("#high_school").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);
				}
			});
		},
		minLength: 1
		});
		
	});
        $(function() {
		$("input#higher_education" ).autocomplete({
			source: function(request, response) {
				$.ajax({ url:$('#base_url').val()+'suggestion/get_higher_education_suggestion_from_user_profile_data',
				data: { term: $("#higher_education").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);
				}
			});
		},
		minLength: 1
		});
		
	});
//    $(function() {
//		$("input#current_location_country" ).autocomplete({
//			source: function(request, response) {
//				$.ajax({ url:$('#base_url').val()+'suggestion/get_current_location_from_user_profile_data',
//				data: { term: $("#current_location_country").val()},
//				dataType: "json",
//				type: "POST",
//				success: function(data){
//					response(data);
//				}
//			});
//		},
//		minLength: 1
//		});
//		
//	});
    $(function() {
		$("input#current_location_state" ).autocomplete({
			source: function(request, response) {
				$.ajax({ url:$('#base_url').val()+'suggestion/get_current_location_state',
				data: { term: $("#current_location_state").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);
				}
			});
		},
		minLength: 1
		});
		
	});
        $(function() {
		$("input#current_location_city" ).autocomplete({
			source: function(request, response) {
				$.ajax({ url:$('#base_url').val()+'suggestion/get_current_location_city',
				data: { term: $("#current_location_city").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);
				}
			});
		},
		minLength: 1
		});
		
	});
//        $(function() {
//		$("input#home_town_country" ).autocomplete({
//			source: function(request, response) {
//				$.ajax({ url:$('#base_url').val()+'suggestion/get_current_location_from_user_profile_data',
//				data: { term: $("#home_town_country").val()},
//				dataType: "json",
//				type: "POST",
//				success: function(data){
//					response(data);
//				}
//			});
//		},
//		minLength: 1
//		});
//		
//	});
        $(function() {
		$("input#home_town_state" ).autocomplete({
			source: function(request, response) {
				$.ajax({ url:$('#base_url').val()+'suggestion/get_home_town_state',
				data: { term: $("#home_town_state").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);
				}
			});
		},
		minLength: 1
		});
		
	});
        $(function() {
		$("input#home_town_city" ).autocomplete({
			source: function(request, response) {
				$.ajax({ url:$('#base_url').val()+'suggestion/get_home_town_city',
				data: { term: $("#home_town_city").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);
				}
			});
		},
		minLength: 1
		});
		
	});
}
function get_advance_search_data(){
     $('#search_result_content_div').animate({
        opacity: 0.25
        }, 100, function() {
       
        });
    var name=$('#name').attr("value");
    var age=$('#age').attr("value");
    var sign=$('#sign').attr("value");
    var gender=$('#gender').attr("value");
    var high_school=$('#high_school').attr("value");
    var higher_education=$('#higher_education').attr("value");

    var current_location_country=$('#current_location_country').attr("value");
    var current_location_state=$('#current_location_state').attr("value");
    var current_location_city=$('#current_location_city').attr("value");

    var home_town_country=$('#home_town_country').attr("value");
    var home_town_state=$('#home_town_state').attr("value");
    var home_town_city=$('#home_town_city').attr("value");
    
    $.ajax({
        type:"post",
        data:{
            name:name,
            age:age,
            sign:sign,
            gender:gender,
            high_school:high_school,
            higher_education:higher_education,
            current_location_country:current_location_country,
            current_location_state:current_location_state,
            current_location_city:current_location_city,
            home_town_country:home_town_country,
            home_town_state:home_town_state,
            home_town_city:home_town_city
            
        },
        url:$('#base_url').val()+"search/advance_search",
        success:function(data){
            $('#search_result_content_div').fadeOut(100,function(){
                 $('#search_result_content_div').css('opacity','1');
                $('#search_result_content_div').html(data);
                $('#search_result_content_div').fadeIn(500);
            });
            
        }
    });
}
</script>
                           


<body>
	 <?php include 'advertise_left.php' ?>
 	 <?php include 'advertise_right.php' ?>
<!--Hidden Tags[Starts]-->
<input type="hidden" id="base_url" value="<?php echo $site_url; ?>" />
<!--Hidden Tags[Ends]-->
 	 <div id="content">
 	 	<div class="infoBar">
 	 		<p>Advanced Search and Comparisons</p>
 	 	</div>
 	 	<div class="as_top_container">
 	 		<div class="as_pp">
                          
                                <span><?php echo $name;?></span><br>
                                <?php  if($profile_picture!=""){ ?>
                                    <img id="user_profile_picture" src="<?php echo base_url().'uploads/'.$profile_picture;?>" 
                                         style="min-height: 135px;
                                                min-width: 125px;
                                                max-height: 135px;
                                                max-width: 125px;"
                                                />
                                <?php }else{ ?>
                                    <img id="user_profile_picture" src="<?php echo base_url().'images/user.png'; ?>"
                                                style="min-height: 135px;
                                                min-width: 125px;
                                                max-height: 135px;
                                                max-width: 125px;"
                                                />
                                <?php } ?>
                        </div>
 	 		<div class="as_q_container">
 	 			<div class="quality">
							<span class="ag_span">Aggrigates</span><br>
							<div class="round_div_violet">
								<p class="round_diff">84%</p>
							</div>
							<div class="progressbar_l" style="background:#5E2590;"><!--violet-->
					    		<div class="bottom_bar_l"></div>
					    		<div id="t" class="i_bar_l_v" style="border-right:1px solid #111;width:70%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf_l"><span class="span_name">Rank Score</span><span class="span_score"><span class="span_score">750</span></span></div>
							</div><br>
							<div class="round_div_purple">
								<p class="round_diff">70%</p>
							</div>
							<div class="progressbar_l" style="background:#EC008C;"><!--pink-->
					    		<div class="bottom_bar_l"></div>
					    		<div id="t" class="i_bar_l_p" style="border-right:1px solid #111;width:75%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf_l"><span class="span_name">Reputation</span><span class="span_score"><span class="span_score">75</span></span></div>
							</div> <br>
							<div class="round_div_orange">
								<p class="round_diff">75%</p>
							</div>
							<div class="progressbar_l" style="background:#EF2E32;"><!--orange-->
					    		<div class="bottom_bar_l"></div>
					    		<div id="t" class="i_bar_l_o" style="border-right:1px solid #111;width:85%"><span class="inside-middle"></span></div>
					    		<div class="top_inf_l"><span class="span_name">Popularity</span><span class="span_score"><span class="span_score">100</span></span></div>
							</div>
				</div>
				<div class="as_q_small">
					<div class="quality"> <!--onclick="this.style.height=(this.style.height=='165px'?'auto':'165px');"-->
							<span class="vi_span">Virtues</span><br>
							<div class="block1">
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--Blue-->
							    <div class="bottom_bar"></div>
							    <div class="i_bar_s" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span></div>
							    <div class="top_inf"><span class="span_name">Reliable</span><span class="span_score"><span class="span_score">85</span></span></div>
							</div> 
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--Blue-->
							    <div class="bottom_bar"></div>
							    <div class="i_bar_s" style="border-right:1px solid #111;width:75%;"><span class="inside-middle"></span></div>
							    <div class="top_inf"><span class="span_name">kind</span><span class="span_score"><span class="span_score">75</span></span></div>
							</div> 
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--Blue-->
							    <div class="bottom_bar"></div>
							    <div class="i_bar_s" style="border-right:1px solid #111;width:60%;"><span class="inside-middle"></span></div>
							    <div class="top_inf"><span class="span_name">Honest</span><span class="span_score"><span class="span_score">60</span></span></div>
							</div> 
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--Blue-->
							    <div class="bottom_bar"></div>
							    <div class="i_bar_s" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span></div>
							    <div class="top_inf"><span class="span_name">Generous</span><span class="span_score"><span class="span_score">85</span></span></div>
							</div>
							</div> 
							<div id="head_slide" class="block2" style="display:none;">
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--Blue-->
							    <div class="bottom_bar"></div>
							    <div class="i_bar_s" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span></div>
							    <div class="top_inf"><span class="span_name">Helpful</span><span class="span_score"><span class="span_score">85</span></span></div>
							</div> 
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--Blue-->
							    <div class="bottom_bar"></div>
							    <div class="i_bar_s" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span></div>
							    <div class="top_inf"><span class="span_name">Helpful</span><span class="span_score"><span class="span_score">85</span></span></div>
							</div> 
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--Blue-->
							    <div class="bottom_bar"></div>
							    <div class="i_bar_s" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span></div>
							    <div class="top_inf"><span class="span_name">Helpful</span><span class="span_score"><span class="span_score">85</span></span></div>
							</div> 
							</div>
							<div class="block3" id="toggle">
								<img id="show_img" style="margin:0 auto;" src="<?php echo base_url(); ?>images/downarw.png" width="25" height="25"/>
							</div>
					</div>
					<div class="quality">
							<span class="tr_span">Traits</span><br>
							<div class="block1">
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--green-->
					    		<div class="bottom_bar"></div>
					    		<div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score"><span class="span_score">85</span></span></div>
							</div> 
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--green-->
					    		<div class="bottom_bar"></div>
					    		<div class="i_bar_g" style="border-right:1px solid #111;width:75%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Funny</span><span class="span_score"><span class="span_score">75</span></span></div>
							</div>
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--green-->
					    		<div class="bottom_bar"></div>
					    		<div class="i_bar_g" style="border-right:1px solid #111;width:60%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Creative</span><span class="span_score"><span class="span_score">60</span></span></div>
							</div>
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--green-->
					    		<div class="bottom_bar"></div>
					    		<div class="i_bar_g" style="border-right:1px solid #111;width:55%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Wise</span><span class="span_score"><span class="span_score">55</span></span></div>
							</div>
							</div>
							<div id="head_slide1" class="block2" style="display:none;">
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--green-->
					    		<div class="bottom_bar"></div>
					    		<div class="i_bar_g" style="border-right:1px solid #111;width:50%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Talketive</span><span class="span_score"><span class="span_score">50</span></span></div>
							</div>
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--green-->
					    		<div class="bottom_bar"></div>
					    		<div class="i_bar_g" style="border-right:1px solid #111;width:50%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Talketive</span><span class="span_score"><span class="span_score">50</span></span></div>
							</div>
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--green-->
					    		<div class="bottom_bar"></div>
					    		<div class="i_bar_g" style="border-right:1px solid #111;width:50%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Talketive</span><span class="span_score"><span class="span_score">50</span></span></div>
							</div>
							<div class="progressbar" style="margin: 7px 0 2px 10px;"><!--green-->
					    		<div class="bottom_bar"></div>
					    		<div class="i_bar_g" style="border-right:1px solid #111;width:50%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Talketive</span><span class="span_score"><span class="span_score">50</span></span></div>
							</div>
							</div>
							<div class="block3" id="toggle1">
								<img id="show_img1" style="margin:0 auto;" src="<?php echo base_url(); ?>images/downarw.png" width="25" height="25"/>
							</div>
					</div>
					<div class="quality">
							<span class="sk_span">Skills</span><br>
							<div class="block1">
							<div class="progressbar"  style="margin: 7px 0 2px 10px;"><!--Yellow-->
					    		<div class="bottom_bar"></div>
					    		<div id="t" class="i_bar_y" style="border-right:1px solid #111;width:75%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Sports</span><span class="span_score"><span class="span_score">75</span></span></div>
							</div> 
							<div class="progressbar"  style="margin: 7px 0 2px 10px;"><!--Yellow-->
					    		<div class="bottom_bar"></div>
					    		<div id="t" class="i_bar_y" style="border-right:1px solid #111;width:90%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Maths</span><span class="span_score"><span class="span_score">90</span></span></div>
							</div>
							<div class="progressbar"  style="margin: 7px 0 2px 10px;"><!--Yellow-->
					    		<div class="bottom_bar"></div>
					    		<div id="t" class="i_bar_y" style="border-right:1px solid #111;width:70%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Arts</span><span class="span_score"><span class="span_score">70</span></span></div>
							</div>
							<div class="progressbar"  style="margin: 7px 0 2px 10px;"><!--Yellow-->
					    		<div class="bottom_bar"></div>
					    		<div id="t" class="i_bar_y" style="border-right:1px solid #111;width:10%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Cooking</span><span class="span_score"><span class="span_score">10</span></span></div>
							</div>
							</div>
							<div id="head_slide2" class="block2" style="display:none;">
								<div class="progressbar"  style="margin: 7px 0 2px 10px;"><!--Yellow-->
					    		<div class="bottom_bar"></div>
					    		<div id="t" class="i_bar_y" style="border-right:1px solid #111;width:70%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Arts</span><span class="span_score"><span class="span_score">70</span></span></div>
							</div>
							<div class="progressbar"  style="margin: 7px 0 2px 10px;"><!--Yellow-->
					    		<div class="bottom_bar"></div>
					    		<div id="t" class="i_bar_y" style="border-right:1px solid #111;width:10%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Cooking</span><span class="span_score"><span class="span_score">10</span></span></div>
							</div>
							<div class="progressbar"  style="margin: 7px 0 2px 10px;"><!--Yellow-->
					    		<div class="bottom_bar"></div>
					    		<div id="t" class="i_bar_y" style="border-right:1px solid #111;width:70%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Arts</span><span class="span_score"><span class="span_score">70</span></span></div>
							</div>
							<div class="progressbar"  style="margin: 7px 0 2px 10px;"><!--Yellow-->
					    		<div class="bottom_bar"></div>
					    		<div id="t" class="i_bar_y" style="border-right:1px solid #111;width:10%;"><span class="inside-middle"></span></div>
					    		<div class="top_inf"><span class="span_name">Cooking</span><span class="span_score"><span class="span_score">10</span></span></div>
							</div>
							</div>
							<div class="block3" id="toggle2">
								<img id="show_img2" style="margin:0 auto;" src="<?php echo base_url(); ?>images/downarw.png" width="25" height="25"/>
							</div>

						</div>
				</div>
 	 		</div>
 	 	</div>
 	 	<div class="as_main">
 	 		<div class="as_left">
 	 			<div class="as_input_bar">
 	 				<p style="margin:0 auto;text-align:center;">Group Member Search Input</p>
 	 			</div>
 	 			<div class="as_input_bar">
 	 				<div class="as_p">
 	 					<p>Name</p>
 	 				</div>
 	 				<div class="as_input">
 	 					<input id="name" type="text">
 	 				</div>
 	 			</div>
 	 			<div class="as_input_bar">
 	 				<div class="as_p">
 	 					<p>Age/Sign</p>
 	 				</div>
 	 				<div class="as_input">
 	 					<select id="age" class="as_input_smaller">
 	 						<option value="" >-</option>
                                                        <?php foreach($age as $rowData){ ?>
                                                            <option value="<?php echo $rowData->value; ?>" ><?php echo $rowData->value; ?></option>
                                                        <?php } ?>
 	 					</select>
 	 					<select id="sign" class="as_input_small">
 	 						<option value="" >-</option>
                                                         <?php foreach($sign as $rowData){ ?>
                                                            <option value="<?php echo $rowData->id; ?>" ><?php echo $rowData->value; ?></option>
                                                        <?php } ?>
 	 					</select>
 	 				</div>
 	 			</div>
 	 			<div class="as_input_bar">
 	 				<div class="as_p">
 	 					<p>Gender</p>
 	 				</div>
 	 				<div class="as_input">
 	 					<select id="gender" class="as_input_large">
                                                         <option value="" >-</option>
 	 						 <?php foreach($gender as $rowData){ ?>
                                                            <option value="<?php echo $rowData->id; ?>" ><?php echo $rowData->value; ?></option>
                                                        <?php } ?>
 	 					</select>
 	 				</div>
 	 			</div>
 	 			<div class="as_input_bar">
 	 				<div class="as_p">
 	 					<p>School</p>
 	 				</div>
 	 				<div class="as_input">
 	 					<input id="high_school" type="text" placeholder="High School"> 
 	 					<input id="higher_education" type="text" placeholder="Higher Education">
 	 				</div>
 	 			</div>
 	 			<div class="as_input_bar">
 	 				<div class="as_p">
 	 					<p>Current Location</p>
 	 				</div>
 	 				<div class="as_input">
 	 					<input id="current_location_country" type="text" placeholder="Country" > 
 	 					<input id="current_location_state" type="text" placeholder="State" >
 	 					<input id="current_location_city" type="text" placeholder="City" >
 	 				</div>
 	 			</div>
 	 			<div class="as_input_bar">
 	 				<div class="as_p">
 	 					<p>Home<br> Town</p>
 	 				</div>
 	 				<div class="as_input">
 	 					<input id="home_town_country" type="text" placeholder="Country" >
 	 					<input id="home_town_state" type="text" placeholder="State" >
 	 					<input id="home_town_city" type="text" placeholder="City" >
 	 				</div>
 	 			</div>
 	 			<div class="as_input_bar">
 	 				<div class="as_p">
 	 					<p>Trait</p>
 	 				</div>
 	 				<div class="as_input">
 	 					<input type="text">
 	 				</div>
 	 			</div>
 	 			<div class="as_input_bar">
 	 				<div class="as_p">
 	 					<p>Order</p>
 	 				</div>
 	 				<div class="as_input">
 	 					<input type="text">
 	 				</div>
 	 			</div>
 	 			<a href="javascript:void(0)"><div class="btn_footer" style="margin:10px 0 10px 7px;">
 	 				Compare Results
 	 			</div></a>
 	 			<a href="javascript:get_advance_search_data()">
 	 				<div class="btn_footer" style="margin:10px 0 10px 80px;">
 	 					View Results
 	 				</div></a>
 	 		</div>
 	 		<div id="search_result_content_div" class="as_middle">
 	 			<div class="as_sr">
                                    <span style="margin:0;">Search Results</span><span>(1)</span> 
 	 			</div>
 	 			<div  class="as_middle_s">
                                    <div class="as_sr_container">
                                            <div class="pe_circle_purple">
                                                    <p class="p_multi">1</p>
                                            </div>
                                            <div class="as_sr_progress">
                                                    <img src="<?php echo base_url(); ?>images/jason_dunn.png" height="45" width="40">
                                                    <span class="as_l_span">Jason Dunn</span><span class="as_s_span">New York</span>
                                                    <br>
                                                    <span class="as_s_para">18/1/2013</span>
                                                    <div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
                                                            <div class="bottom_bar"></div>
                                                            <div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
                                                            </div>
                                                            <div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
                                                    </div>
                                            </div>
                                            <a style="text-decoration:none;" href=""><div class="as_circle_plus">
                                                    <span style="font-weight:bold;font-size:25px;margin:0 0 0 4px;">+</span>
                                            </div></a>
                                    </div>
 	 			</div>
 	 		</div>
 	 		<div class="as_right">
 	 			<div class="as_sr">
 	 			<span style="margin:0;">Search Results</span><span>(25)</span> 
 	 			</div>
 	 			<div class="as_right_s">
 	 			<div class="as_sr_s_container">
 	 				<div class="pe_circle_purple">
						<p class="p_multi">1</p>
					</div>
					<div class="as_sr_progress">
						<img src="<?php echo base_url(); ?>images/jason_dunn.png" height="45" width="40">
						<span class="as_l_span">Jason Dunn</span><span class="as_s_span">New York</span>
						<br>
						<span class="as_s_para">18/1/2013</span>
						<div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
							<div class="bottom_bar"></div>
							<div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
							</div>
							<div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
						</div>
					</div>
 	 			</div>
 	 			<div class="as_sr_s_container">
 	 				<div class="pe_circle_purple">
						<p class="p_multi">1</p>
					</div>
					<div class="as_sr_progress">
						<img src="<?php echo base_url(); ?>images/jason_dunn.png" height="45" width="40">
						<span class="as_l_span">Jason Dunn</span><span class="as_s_span">New York</span>
						<br>
						<span class="as_s_para">18/1/2013</span>
						<div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
							<div class="bottom_bar"></div>
							<div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
							</div>
							<div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
						</div>
					</div>
 	 			</div>
 	 			<div class="as_sr_s_container">
 	 				<div class="pe_circle_purple">
						<p class="p_multi">1</p>
					</div>
					<div class="as_sr_progress">
						<img src="<?php echo base_url(); ?>images/jason_dunn.png" height="45" width="40">
						<span class="as_l_span">Jason Dunn</span><span class="as_s_span">New York</span>
						<br>
						<span class="as_s_para">18/1/2013</span>
						<div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
							<div class="bottom_bar"></div>
							<div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
							</div>
							<div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
						</div>
					</div>
 	 			</div>
 	 			<div class="as_sr_s_container">
 	 				<div class="pe_circle_purple">
						<p class="p_multi">1</p>
					</div>
					<div class="as_sr_progress">
						<img src="<?php echo base_url(); ?>images/jason_dunn.png" height="45" width="40">
						<span class="as_l_span">Jason Dunn</span><span class="as_s_span">New York</span>
						<br>
						<span class="as_s_para">18/1/2013</span>
						<div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
							<div class="bottom_bar"></div>
							<div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
							</div>
							<div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
						</div>
					</div>
 	 			</div>
 	 			<div class="as_sr_s_container">
 	 				<div class="pe_circle_purple">
						<p class="p_multi">1</p>
					</div>
					<div class="as_sr_progress">
						<img src="<?php echo base_url(); ?>images/jason_dunn.png" height="45" width="40">
						<span class="as_l_span">Jason Dunn</span><span class="as_s_span">New York</span>
						<br>
						<span class="as_s_para">18/1/2013</span>
						<div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
							<div class="bottom_bar"></div>
							<div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
							</div>
							<div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
						</div>
					</div>
 	 			</div>
 	 			<div class="as_sr_s_container">
 	 				<div class="pe_circle_purple">
						<p class="p_multi">1</p>
					</div>
					<div class="as_sr_progress">
						<img src="<?php echo base_url(); ?>images/jason_dunn.png" height="45" width="40">
						<span class="as_l_span">Jason Dunn</span><span class="as_s_span">New York</span>
						<br>
						<span class="as_s_para">18/1/2013</span>
						<div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
							<div class="bottom_bar"></div>
							<div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
							</div>
							<div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
						</div>
					</div>
 	 			</div>
 	 			<div class="as_sr_s_container">
 	 				<div class="pe_circle_purple">
						<p class="p_multi">1</p>
					</div>
					<div class="as_sr_progress">
						<img src="<?php echo base_url(); ?>images/jason_dunn.png" height="45" width="40">
						<span class="as_l_span">Jason Dunn</span><span class="as_s_span">New York</span>
						<br>
						<span class="as_s_para">18/1/2013</span>
						<div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
							<div class="bottom_bar"></div>
							<div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
							</div>
							<div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
						</div>
					</div>
 	 			</div>
 	 			<div class="as_sr_s_container">
 	 				<div class="pe_circle_purple">
						<p class="p_multi">1</p>
					</div>
					<div class="as_sr_progress">
						<img src="<?php echo base_url(); ?>images/jason_dunn.png" height="45" width="40">
						<span class="as_l_span">Jason Dunn</span><span class="as_s_span">New York</span>
						<br>
						<span class="as_s_para">18/1/2013</span>
						<div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
							<div class="bottom_bar"></div>
							<div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
							</div>
							<div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
						</div>
					</div>
 	 			</div>
 	 			<div class="as_sr_s_container">
 	 				<div class="pe_circle_purple">
						<p class="p_multi">1</p>
					</div>
					<div class="as_sr_progress">
						<img src="<?php echo base_url(); ?>images/jason_dunn.png" height="45" width="40">
						<span class="as_l_span">Jason Dunn</span><span class="as_s_span">New York</span>
						<br>
						<span class="as_s_para">18/1/2013</span>
						<div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
							<div class="bottom_bar"></div>
							<div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
							</div>
							<div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
						</div>
					</div>
 	 			</div>
 	 			<div class="as_sr_s_container">
 	 				<div class="pe_circle_purple">
						<p class="p_multi">1</p>
					</div>
					<div class="as_sr_progress">
						<img src="<?php echo base_url(); ?>images/jason_dunn.png" height="45" width="40">
						<span class="as_l_span">Jason Dunn</span><span class="as_s_span">New York</span>
						<br>
						<span class="as_s_para">18/1/2013</span>
						<div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
							<div class="bottom_bar"></div>
							<div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
							</div>
							<div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
						</div>
					</div>
 	 			</div>
 	 			
 	 		</div>
 	 		<a href="">
 	 				<div class="btn_footer" style="margin:50px 0 10px 165px;">
 	 					Compare Results
 	 				</div></a>
 	 		</div>
 	 	</div>

 	 </div>

</body> 
<script>
    auto_suggestion();
    </script>
 <?php include 'footer.php' ?>               