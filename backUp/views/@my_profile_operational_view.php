<?php include 'header.php' ?>
<!--For Tabs-->

<script type="text/javascript" src="<?php echo base_url(); ?>jQuery/tytabs.jquery.min.js"></script>
<style>
	.editBioDataDiv{
		color:black;
		text-align:left;
		background:white;border: 1px solid #111111;
		float: left;
		weight: 17px;
		margin: 0;
		width: 175px;
                
											    
	}
</style>

<script type="text/javascript" >
        function submit_update_about_me_data(){
            var about_me_text=$('#user_about_me_text').val();
             $.ajax({
				type:"POST",
                                data:{about_me_text:about_me_text},
				url:$('#site_url').attr("value")+"user_about_me/submit_update_about_me_data",
				success:function(data){
                                    var resp=data.split(";");
                                    if(resp[1]=="True"){
                                        $('#user_about_me_content').fadeOut(500,function(){
                                                            $('#user_about_me_content').html(about_me_text);
                                                            $('#user_about_me_content').fadeIn(100);
                                                            
                                                        });
                                                        console.log(about_me_text);
                                    }else if(resp[1]=="False"){
                                          
                                        alert("Internal Server Error.. Update unsuccessful");
                                    }
				}

		});
        }
        function load_user_about_me_update_view(){
            $.ajax({
				type:"POST",
				url:$('#site_url').attr("value")+"user_about_me/get_user_about_me_update_view",
				success:function(data){
						$('#user_about_me_content').fadeOut(500,function(){
                                                            
                                                            $('#user_about_me_content').html(data);
                                                            $('#user_about_me_content').fadeIn(100);
                                                            $('#user_about_me_text').focus();
                                                        });
				}

		});
        }
        function disabled_edit_bio_data_anchor(){
             $('#edit_bio_data_anchor').attr("disabled","disabled");
        }
        function remove_disabled_from_edit_bio_data_anchor(){
             $('#edit_bio_data_anchor').removeAttr("disabled");
        }
        function edit_operation(){
            var state= $('#edit_bio_data_anchor').attr("disabled");
            var keyWord= $('#edit_bio_data_anchor').html();
            if(state!="disabled" && keyWord=="Edit"){
                show_all_edit_anchor();
                $('#edit_bio_data_anchor').fadeOut(500,function(){
                        $('#edit_bio_data_anchor').html("Done");
                        $('#edit_bio_data_anchor').fadeIn(500,function(){});
                    });
            }else if(state!="disabled" && keyWord=="Done"){
                 hide_all_edit_anchor();
                $('#edit_bio_data_anchor').fadeOut(500,function(){
                        $('#edit_bio_data_anchor').html("Edit");
                        $('#edit_bio_data_anchor').fadeIn(500,function(){});
                    });
            }
        }
        function show_all_edit_anchor(){
		$('.bio_data_process').fadeOut(500,function(){
			$('.bio_data_edit_anchor').fadeIn(500,function(){});
			});
		
	}
	function show_edit_anchor(prefix){
		$('#'+prefix+'_anchor').fadeIn(500,function(){});
		}
	function hide_all_edit_anchor(){
		$('.bio_data_edit_anchor').fadeOut(500,function(){});
		}
	function hide_edit_anchor(prefix){
		$('#'+prefix+'_anchor').fadeOut(500,function(){});
		}
	
	function show_process(prefix){
		$('#'+prefix+'_anchor').fadeOut(10,function(){
			$('#'+prefix+'_process').fadeIn(10);
		});
	}
	function hide_process(prefix){
		$('#'+prefix+'_process').hide();
               
	}
	function show_login_form(){	
			$('#search_form_div').fadeOut(500,function(){
				$('#login_form_div').fadeIn(500,function(){});
				$('#