<?php include 'admin_header.php' ?>
<script>
    
    function load_subject_and_message_By_type(mail_type){
        $.ajax({
                type:"post",
                data:{mail_type:mail_type},
                url:$('#site_url').val()+"mail_template/get_subject_and_message_by_type",
                success:function(data){
                    
                    $('#update_mail_content_view').hide();
                    $('#update_mail_content_view').html(data);
                    $('#update_mail_content_view').fadeIn(500,function(){
                        $('#message').focus();
                    });
                }
        });
    }
    function  submit_update_mail_template_by_type(){
        var subject=$('#subject').attr("value");
        var message=$('#message').attr("value");
        var type=$('#type').attr("value");
        $.ajax({
                type:"post",
                data:{
                    subject:subject,
                    message:message,
                    type:type
                },
                url:$('#site_url').val()+"mail_template/submit_update_mail_template_by_type",
                success:function(data){
                    var resp=data.split(';');
                    if(resp[1]="True"){
                        $('#update_mail_content_view').fadeOut(500,function(){
                                $('#update_mail_content_view').css("background","yellow");
                                $('#update_mail_content_view').css("color","black");
                                $('#update_mail_content_view').css("text-align","center");
                                $('#update_mail_content_view').html(resp[2]);
                                $('#update_mail_content_view').fadeIn(500).delay(2000).fadeOut(500,
                                    function(){
                                            $('#update_mail_content_view').html("");
                                            $('#update_mail_content_view').css("background","white");
                                            $('#update_mail_content_view').css("color","white");
                                            $('#update_mail_content_view').css("text-align","");
                                    }
                                );
                            });
                        
                    }else if(resp[1]="False"){
                         $('#update_mail_content_view').fadeOut(500,function(){
                                $('#update_mail_content_view').css("background","yellow");
                                $('#update_mail_content_view').html(resp[2]);
                                $('#update_mail_content_view').fadeIn(500).delay(2000).fadeOut(500,
                                    function(){
                                            $('#update_mail_content_view').html("");
                                    }
                                );
                            });
                    }
                   
                }
        });
    }
    function cancel_submit_update_mail_template_by_type(){
        $('#update_mail_content_view').fadeOut(500,function(){
           $('#update_mail_content_view').html(""); 
          $('#signup_div').focus();
        });
    }
</script>
<?php include 'admin_dashboard.php' ?>

<div class="admin_panel">
	<?php include 'top_container.php' ?>
	<div class="admin_container">
		<div class="criteria">
			<div class="head_div">
				<p class="head_div_desc">Message Templates</p>
			</div>
			<div id="signup_div" class="message_temp">
				<p>1.</p>
				<p>Signup Email</p>
				<div class="btn_message_edit">
						<a href="javascript:load_subject_and_message_By_type('sign_up')"><span>Edit</span></a>
				</div>
			</div>
			<div class="message_temp">
				<p>2.</p>
				<p>Forgot Email</p>
				<div class="btn_message_edit">
					<a href="javascript:load_subject_and_message_By_type('forgot_email')"><span>Edit</span></a>
				</div>
			</div>
			<div class="message_temp">
				<p>3.</p>
				<p>Invite a friend</p>
				<div class="btn_message_edit">
					<a href="javascript:load_subject_and_message_By_type('invite_a_friend')"><span>Edit</span></a>
				</div>
			</div>
			<div class="message_temp">
				<p>4.</p>
				<p>Gossip received</p>
				<div class="btn_message_edit">
                                    <a href="javascript:load_subject_and_message_By_type('gossip_received')"><span>Edit</span></a>
				</div>
			</div>
			<div class="message_temp">
				<p>5.</p>
				<p>Favourite gossiped about</p>
				<div class="btn_message_edit">
                                    <a href="javascript:load_subject_and_message_By_type('favourite_gossiped_about')"><span>Edit</span></a>
				</div>
			</div>
			<div class="message_temp">
				<p>6.</p>
				<p>Maintainace Mode page</p>
				<div class="btn_message_edit">
                                    <a href="javascript:load_subject_and_message_By_type('maintainace_mode_page')"><span>Edit</span></a>
                                </div>
			</div>

		</div><br><br><br>

		<div id="update_mail_content_view">
                </div>
			


	</div>
</div>	
<?php include 'footer.php' ?>
<!-- Hidden Tags [Starts]-->
<input type="hidden" id="site_url" value="<?php echo $site_url; ?>"
<!-- Hidden Tags [Ends]-->