<div class="as_sr">
    <span style="margin:0;">Search Results</span><span>(<?php echo sizeof($search_data); ?>)</span> 
</div>
<div class="as_middle_s">
    <?php
    $z=1;
    if (sizeof($search_data)>0){
        foreach($search_data as $rowData){ ?>
        <div class="as_sr_container">
            <div class="pe_circle_purple">
                    <p class="p_multi"><?php echo $z; ?></p>
            </div>
            <div class="as_sr_progress">
                <?php if($rowData->pic_path!=""){ ?>
                    <img src="<?php echo base_url().'uploads/'.$rowData->pic_path; ?>" height="45" width="40">
                <?php }else{ ?>
                       <img src="<?php echo base_url().'images/user.png'; ?>" height="45" width="40">
                <?php } ?>
                    <span class="as_l_span"><?php echo $rowData->f_name." ". $rowData->l_name;?></span><span class="as_s_span">New York</span>
                    <br>
                    <span class="as_s_para">18/1/2013</span>
                    <div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
                            <div class="bottom_bar"></div>
                            <div class="i_bar_g" style="border-right:1px solid #111;width:85%;"><span class="inside-middle"></span>
                            </div>
                            <div class="top_inf"><span class="span_name">Hardworking</span><span class="span_score">85</span></div>
                    </div>
            </div>
            <a style="text-decoration:none;" href=""><div class="as_circle_plus_act">
                    <span style="font-weight:bold;font-size:25px;margin:0 0 0 4px;">+</span>
            </div></a>
        </div>
    <?php
       
        $z++;
        } 
    }else{ ?>
          <div class="as_sr">
                <span style="margin:0;">  No Result Found</span> 
           </div>
        
   <?php } ?>
</div>