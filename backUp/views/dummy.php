   function get_home_town_state(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_home_town_state($term) as $rowData){
                    if($rowData!=""){	
                        array_push($keywords,$rowData);
                    }
		}
		echo json_encode($keywords);
	}
        function get_home_town_city(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_home_town_city($term) as $rowData){
                    if($rowData!=""){	
                        array_push($keywords,$rowData);
                    }
		}
		echo json_encode($keywords);
	}