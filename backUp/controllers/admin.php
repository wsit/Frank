<?php
class admin extends CI_Controller{
    private $sign_out;
    private $pageData;
    private $site_url;
    private $u_id;
    function __construct(){
    	parent::__construct();
    	$this->load->helper('url');
        $this->pageData=array();
	include_once(APPPATH.'controllers/common_site_setting.php');
	include_once(APPPATH.'controllers/class_file/site_url.php');
        if($this->session->userdata('admin_inf')){
           $session_data=$this->session->userdata('admin_inf');
           $this->u_id=$session_data['u_id'];
        }else{
            redirect(base_url().'authentication');
        }
	$this->pageData['sign_out']=&$this->sign_out;
        $this->sign_out=TRUE;
    }
    function index(){
        
       redirect(base_url().'authentication');
    }
    function login()
    {
        $this->load->view('admin_login_view'); 
    }
    
    function dashboard(){
        $this->load->view('admin_dashboard_view',  $this->pageData); 
    }
    function members(){
        $this->load->library('pagination');

        $config['base_url'] = base_url().'admin/members';
        $config['total_rows'] = 200;
        $config['per_page'] = 20;

        $this->pagination->initialize($config);


        $this->load->model("m_user_login");
        $this->load->model("m_user_login_history");
        $star_row=0;
        $total_row=$this->m_user_login->get_row_size();
        $config['base_url'] = base_url().'admin/members';
        $config['total_rows'] =$total_row;
        $config['per_page'] = 2;
        
       
        if($this->uri->segment(3)!=""){
            $star_row=intval($this->uri->segment(3));
        }
        
        $this->pageData['user_inf']=$this->m_user_login->j_get_user_inf_for_admin($star_row,2);
        foreach($this->pageData['user_inf'] as $rowData){
            $this->pageData['login_count'][$rowData->user_id]=$this->m_user_login_history->get_login_count($rowData->user_id);
        }
      
        $this->pageData['start_row']=$star_row+1;
        
        $this->pagination->initialize($config);
       
        $this->load->view('admin_member_view',$this->pageData);
    }
    function website_settings(){
        include_once(APPPATH.'controllers/class_file/c_admin.php');
        $c_admin_obj=new c_admin();
        
        $this->pageData+=$c_admin_obj->get_dashboard(2);
       
        $this->pageData['ci_site_url']=$this->site_url.'/frank/';
        $this->load->view('admin_website_settings_view',$this->pageData);
    
    }
   
    function advertisers(){
         $this->load->view('admin_advertiser_view',$this->pageData);
    }
    function message_templates(){
        
        $this->load->view('admin_message_template_view',$this->pageData);
    }
    function traits(){
        $this->load->view('admin_traits_view',$this->pageData);
    }
    function algorithm(){
         $this->load->view('admin_algorithm_view',$this->pageData);
    }
    function static_pages_content(){
        $this->load->view('admin_static_pages_views',  $this->pageData);
    }
    function website_settings_submit_update_data(){
       
        $this->site_title=trim($this->input->post('site_title'));
   
        $this->homepage_title=trim($this->input->post('homepage_title'));
        $this->site_url=trim($this->input->post('site_url'));
        $this->admin_email_address=trim($this->input->post('admin_email_address'));
        $this->site_description=trim($this->input->post('site_description'));
        
        include_once(APPPATH.'controllers/class_file/c_admin.php');
        $c_admin_obj=new c_admin();
        $c_admin_obj->set_site_title($this->site_title);
        $c_admin_obj->set_homepage_title($this->homepage_title);
        $c_admin_obj->set_site_url($this->site_url);
        $c_admin_obj->set_admin_email_address($this->admin_email_address);
        $c_admin_obj->set_site_description($this->site_description);
        
        if($c_admin_obj->website_settings_submit_update_data()){
            echo  ';True;global_notification;Settings Saved;';
        }
        else{
            echo  ";False;global_notification;Server Error;";
        }
    }
    function view_user_profile()
    {
        $this->u_id=$this->input->get('u_id');
        include_once(APPPATH.'controllers/class_file/c_information.php');
   	$c_information_obj=new c_information();
   	$this->pageData+=$c_information_obj->get_user_infomation_by_u_id($this->u_id);
        
        $this->pageData['sign_in']=FALSE;
        $this->pageData['sign_up']=FALSE;
        $this->pageData['sign_out']=FALSE;
        $this->pageData['search']=FALSE;
        $this->pageData['home']=FALSE;
        
   	$this->load->view('admin_view_user_profile_view',$this->pageData);
    }
    function sign_out(){
        $this->session->sess_destroy();
	redirect(base_url().'authentication');
    }
    
}
?>