<?php
class my_profile extends CI_Controller{ 
	private $u_id;
	private $pageData;
	private $site_url;
        
        private $sign_in;
        private $sign_up;
        private $sign_out;
        private $search;
        private $home;
   
  function __construct(){
	parent::__construct();
	$this->load->helper('url');
        $this->pageData=array();
	include_once(APPPATH.'controllers/common_site_setting.php');
	include_once(APPPATH.'controllers/class_file/site_url.php');
        include_once(APPPATH.'controllers/class_file/initailize_header_operation.php');
	include_once(APPPATH.'controllers/load_common_properties_of_user.php');
        
         $this->sign_in=FALSE;
         $this->sign_up=FALSE;
         $this->sign_out=TRUE;
         $this->search=TRUE;
         $this->home=FALSE;
	
   }
   function index(){
        $this->load->model('m_uesr_profile_data_hidden');
        
        include_once(APPPATH.'controllers/class_file/c_information.php');
   	$c_information_obj=new c_information();
      
        $this->pageData['gender']="";
        $this->pageData['birthday']="";
        $this->pageData['location']="";
        $this->pageData['home_town']="";
        $this->pageData['high_school']="";
        $this->pageData['higher_education']="";
        $this->pageData['work_place']="";
        $this->pageData['organization']="";
        foreach($this->m_uesr_profile_data_hidden->get_all_profule_data_hiden($this->u_id) as $rowData)
        {
            $this->pageData['gender']=$rowData->gender;
            $this->pageData['birthday']=$rowData->birthday;
            $this->pageData['location']=$rowData->location;
            $this->pageData['home_town']=$rowData->home_town;
            $this->pageData['high_school']=$rowData->high_school ;
            $this->pageData['higher_education']=$rowData->higher_education;
            $this->pageData['work_place']=$rowData->work_place;
            $this->pageData['organization']=$rowData->organization;
        }
   	$this->pageData+=$c_information_obj->get_user_infomation_by_u_id($this->u_id);
   	$this->load->view('my_profile_operational_view',$this->pageData);
   }		
   function change_profile_picture_by_u_id(){
        include_once(APPPATH.'controllers/class_file/c_user_basic_info.php');
         $pic_path=$this->input->post('profile_picture');
        $c_user_basic_info_obj=new c_user_basic_info();
            
        $c_user_basic_info_obj->set_u_id($this->u_id);
        $c_user_basic_info_obj->set_pic_path($pic_path);
        
        $c_user_basic_info_obj->updateRow_BY_id($this->u_id);
            
   }
   function sign_out(){
		$this->session->sess_destroy();
		redirect(base_url());
   }

}