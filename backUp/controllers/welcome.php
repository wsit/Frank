<?php
class welcome extends CI_Controller{
	private $pageData;
	private $siteUrl;
	private $u_id;
        
        private $sign_in;
        private $sign_up;
        private $sign_out;
        private $search;
        private $home;
         
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
                $this->pageData=array();
		include_once(APPPATH.'controllers/common_site_setting.php');
		include_once(APPPATH.'controllers/class_file/site_url.php');
                include_once(APPPATH.'controllers/class_file/initailize_header_operation.php');
                
		if($this->session->userdata('user_login_session')!=null){
                    redirect(base_url().'my_profile');
                }
                $this->sign_in=TRUE;
                $this->sign_up=TRUE;
                $this->sign_out=FALSE;
                $this->search=TRUE;
                $this->home=FALSE;
	}
	function index(){
		
		$this->load->view('index',$this->pageData);
	}
}