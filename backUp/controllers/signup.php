<?php
class signup extends CI_Controller{
	private $pageData;
	private $site_url;
        
         
        private $sign_in;
        private $sign_up;
        private $sign_out;
        private $search;
        private $home;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
                $this->pageData=array();
		include_once(APPPATH.'controllers/class_file/site_url.php');
                include_once(APPPATH.'controllers/class_file/initailize_header_operation.php');
        
                $this->sign_in=FALSE;
                $this->sign_up=FALSE;
                $this->sign_out=FALSE;
                $this->search=FALSE;
		$this->home=FALSE;
	}
	function index(){
		$this->load->model('m_developer_year');
		$this->load->model('m_developer_month');
		$this->load->model('m_developer_day');
        $this->load->model('m_developer_gender');
		$this->pageData['all_year']=$this->m_developer_year->get_all_year();
		$this->pageData['all_month']=$this->m_developer_month->get_all_name();
		$this->pageData['all_day']=$this->m_developer_day->get_day_all();
        $this->pageData['all_gender']=$this->m_developer_gender->get_all();
		$this->load->view('signup_view',$this->pageData);
	}
	function submit_basic_info()
	{
		    include_once(APPPATH.'controllers/class_file/c_user_login.php');
		   
		    $this->load->model('m_user_login');
		    $this->load->model('m_developer_activation');
                    $this->load->model("m_uesr_profile_data_hidden");
		    $user_login_obj=new c_user_login();
		   
		    
		
		    $email=trim($this->input->post('u_email'));
		    $password=trim($this->input->post('password'));
		    $activation_key = substr(md5(microtime()),rand(0,26),10);
		    
		    if(!$this->m_user_login->is_exist($email)){
				
                                $user_login_obj->set_u_email($email);
				$user_login_obj->set_password($password);
				$user_login_obj->set_activation_key($activation_key);
				$user_login_obj->set_activation($this->m_developer_activation->get_id_by_value('inactive'));
				if ($user_login_obj->insertRow()){
					
					$u_id=$this->m_user_login->get_id_by_email($email);
					
					include_once(APPPATH.'controllers/class_file/c_session_signup.php');
					$user_session_obj=new c_session_signup();
					$user_session_obj->create_session_for_sign_up($u_id, $email);
					
					unset($user_session_obj);
					unset($user_login_obj);
 //   				[Sending Email To User]

					include_once(APPPATH.'controllers/class_file/custom_mail.php');
					
					$custom_mail=new custom_mail();
					$custom_mail->send_user_account_activation($email,$activation_key);
					unset($custom_mail);
//					[Basic Info tbl{user_basic_info}]
					
					include_once(APPPATH.'controllers/class_file/c_user_basic_info.php');
					
					$user_basic_info_obj=new c_user_basic_info();
					
					$mm=$this->input->post('month');
			   		$dd=$this->input->post('day');
					$yy=$this->input->post('year');
					
                                        $f_name=trim($this->input->post('f_name'));
					$l_name=trim($this->input->post('l_name'));
					$gender=trim($this->input->post('gender'));
					$birthday=$yy.'-'.$mm.'-'.$dd;
					$pic_path=trim($this->input->post('pic_path'));
					
					$user_basic_info_obj->set_u_id($u_id);
					$user_basic_info_obj->set_f_name($f_name);
					$user_basic_info_obj->set_l_name($l_name);
					$user_basic_info_obj->set_gender($gender);
					$user_basic_info_obj->set_pic_path($pic_path);
					$user_basic_info_obj->set_birthday($birthday);
					$user_basic_info_obj->insertRow();
					
					unset($user_basic_info_obj);

//					[Profile Data tbl{user_profile_data}]
					
					include_once(APPPATH.'controllers/class_file/c_user_profile_data.php');
					$user_profile_data_obj=new c_user_profile_data();
					
					
				    $user_profile_data_obj->set_u_id($u_id);
					$user_profile_data_obj->set_current_location_1('');
					$user_profile_data_obj->set_current_location_2('');
					$user_profile_data_obj->set_home_town_1('');
					$user_profile_data_obj->set_home_town_2('');
					$user_profile_data_obj->set_organization_1('');
					$user_profile_data_obj->set_organization_2('');
					$user_profile_data_obj->set_high_school('');
					$user_profile_data_obj->set_higher_education_1('');
					$user_profile_data_obj->set_higher_education_2('');
					$user_profile_data_obj->set_workplace_1('');
					$user_profile_data_obj->set_workplace_2('');
					$user_profile_data_obj->insertRow();
					
					unset($user_profile_data_obj);
					
//					[Preference Strring tbl{user_profile_data}]

					include_once(APPPATH.'controllers/class_file/c_user_profile_settings.php');
		
					$user_preference_settings_obj=new c_user_profile_settings();
					
					$user_preference_settings_obj->set_u_id($u_id);
					$user_preference_settings_obj->set_birthday_hidden(0);
					$user_preference_settings_obj->set_home_page(0);
					$user_preference_settings_obj->set_col_security(0);
					$user_preference_settings_obj->set_gossip(0);
					$user_preference_settings_obj->set_notification(0);
					
					$user_preference_settings_obj->insertRow();
					
					unset($user_preference_settings_obj);
					
//					[User Invitation tbl{user_invitation}]
	
					include_once(APPPATH.'controllers/class_file/c_user_invitation.php');
					$friend_name=$this->input->post('friend_name');
					$friend_email=$this->input->post('friend_email');
					$user_invitation_obj=new c_user_invitation();
					
					$user_invitation_obj->set_u_id($u_id);
					$user_invitation_obj->set_friend_email('');
					$user_invitation_obj->set_friend_name('');
		
					$user_invitation_obj->insertRow();
					
					unset($user_invitation_obj);
                                        
//					[User About Me tbl{user_about_me}]
                                        include_once(APPPATH.'controllers/class_file/c_user_about_me.php');
					
                                        $c_user_about_me_obj=new c_user_about_me();
                                        
                                        $c_user_about_me_obj->set_u_id($u_id);
                                        $c_user_about_me_obj->set_about_me("");
                                        
                                        $c_user_about_me_obj->insertRow();
                                        unset($c_user_about_me_obj);
//                             		[m_uesr_profile_data_hidden tbl{m_uesr_profile_data_hidden}]
                                        
                                       
                                        $insert_data=array(
                                            'u_id'=>intval($u_id)
                                        );
                                      
                                        $this->m_uesr_profile_data_hidden->insert_data($insert_data);
					echo ";True;";	
				}
				else{ 
					echo ";False;Server Error";
				}
			 }else{
			    echo ";False;u_email;".$email." belongs to an existing account";
			 }
	}
	
	function update_profile_data()
	{
		include_once(APPPATH.'controllers/class_file/c_session_signup.php');
		
		$user_session_signup=new c_session_signup();
	  	$u_id=$user_session_signup->get_session_for_sign_up_u_id();
	  	unset($user_session_signup);
	  	
		include_once(APPPATH.'controllers/class_file/c_user_profile_data.php');
		$user_profile_data_obj=new c_user_profile_data();
		
		$current_location_1=$this->input->post('current_location_1');
		$current_location_2=$this->input->post('current_location_2');
		$home_town_1=$this->input->post('home_town_1');
		$home_town_2=$this->input->post('home_town_2');
		$organization_1=$this->input->post('organization_1');
		$organization_2=$this->input->post('organization_2');
		$high_school=$this->input->post('high_school');
		$higher_education_1=$this->input->post('higher_education_1');
	    $higher_education_2=$this->input->post('higher_education_2');
	    $workplace_1=$this->input->post('workplace_1');
	    $workplace_2=$this->input->post('workplace_2');
		
	    
	    
		$user_profile_data_obj->set_current_location_1($current_location_1);
		$user_profile_data_obj->set_current_location_2($current_location_2);
		$user_profile_data_obj->set_home_town_1($home_town_1);
		$user_profile_data_obj->set_home_town_2($home_town_2);
		$user_profile_data_obj->set_organization_1($organization_1);
		$user_profile_data_obj->set_organization_2($organization_2);
		$user_profile_data_obj->set_high_school($high_school);
		$user_profile_data_obj->set_higher_education_1($higher_education_1);
		$user_profile_data_obj->set_higher_education_2($higher_education_2);
		$user_profile_data_obj->set_workplace_1($workplace_1);
		$user_profile_data_obj->set_workplace_2($workplace_2);
		
		if($user_profile_data_obj->updateRow_BY_u_id($u_id)){
			echo ";True;";
		}
		else{
			echo ";False;";
		}		
	}
	
	function update_preference_strring() {
		
		include_once(APPPATH.'controllers/class_file/c_session_signup.php');
		
		$user_session_signup=new c_session_signup();
	  	$u_id=$user_session_signup->get_session_for_sign_up_u_id();
	  	unset($user_session_signup);
	  	
		include_once(APPPATH.'controllers/class_file/c_user_profile_settings.php');
		
		$user_preference_settings_obj=new c_user_profile_settings();
		
		$birthday_hidden=$this->input->post('birthday_hidden');
		$home_page=$this->input->post('home_page');
		$security=$this->input->post('security');
		$gossip=$this->input->post('gossip');
		$notification=$this->input->post('notification');
		
		
		$user_preference_settings_obj->set_birthday_hidden($birthday_hidden);
		$user_preference_settings_obj->set_home_page($home_page);
		$user_preference_settings_obj->set_col_security($security);
		$user_preference_settings_obj->set_gossip($gossip);
		$user_preference_settings_obj->set_notification($notification);
		
		if($user_preference_settings_obj->updateRow_BY_u_id($u_id))
			echo ";True;";
		else
			echo ";False;";
	}
	function update_invite_friends_data()  
	{
		include_once(APPPATH.'controllers/class_file/c_session_signup.php');
		
		$user_session_signup=new c_session_signup();
	  	$u_id=$user_session_signup->get_session_for_sign_up_u_id();
	  	unset($user_session_signup);
	  	
		include_once(APPPATH.'controllers/class_file/c_user_invitation.php');
		$friend_name=$this->input->post('friend_name');
		$friend_email=$this->input->post('friend_email');
		$user_invitation_obj=new c_user_invitation();
		
	
		$user_invitation_obj->set_friend_email($friend_name);
		$user_invitation_obj->set_friend_name($friend_email);
		
		if($user_invitation_obj->updateRow_BY_u_id($u_id))
		{
			echo ";True;";
		}else{
			echo ";False;";
		}
	}
	function upload_file(){
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|png|jpeg|jpg';
		$config['max_size']	= '1024';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);
		$data='';
		
		if ( ! $this->upload->do_upload("images")){
				
				$data['raw_name']='';
				$data['file_ext']='';
				$resp=str_replace("<p>", "", str_replace("</p>", "", $this->upload->display_errors()));
				echo ";False;".$resp.";";
		}
		else{
				$data=$this->upload->data();
				$pic_path=$data['raw_name'].$data['file_ext'];
				echo ";True;".$pic_path.";";
		}
		
	}
}