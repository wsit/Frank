<?php

class search extends CI_Controller{
  
    private $u_id;
    private $pageData;
    private $site_url;
    
    private $sign_in;
    private $sign_up;
    private $sign_out;
    private $search;
    private $home;
    
    function __construct(){
          parent::__construct();
          $this->load->helper('url');
          $this->pageData=array();
          include_once(APPPATH.'controllers/common_site_setting.php');
          include_once(APPPATH.'controllers/class_file/site_url.php');
          include_once(APPPATH.'controllers/class_file/initailize_header_operation.php');
          include_once(APPPATH.'controllers/load_common_properties_of_user.php');
          $this->sign_in=FALSE;
          $this->sign_up=FALSE;
          $this->sign_out=TRUE;
          $this->search=FALSE;
          $this->home=FALSE;
     }
    function index(){
        $this->load->model("m_developer_age");
        $this->load->model("m_developer_sign");
        $this->load->model("m_developer_gender");
        $this->load->model("m_user_basic_info");
        $this->pageData['profile_picture']=$this->m_user_basic_info->get_pic_path_by_u_id($this->u_id);
        $this->pageData['name']=$this->m_user_basic_info->get_name_by_u_id($this->u_id);
        $this->pageData['age']=$this->m_developer_age->get_all();
        $this->pageData['sign']=$this->m_developer_sign->get_all();
        $this->pageData['gender']=$this->m_developer_gender->get_all();
        $this->load->view('advanced_search_view',$this->pageData);
    } 
    function advance_search(){
        $this->load->model('m_developer_sign');
        
        $name=trim($this->input->post('name'));
        $age=trim($this->input->post('age'));
        $sign_id=trim($this->input->post('sign'));
        $gender=trim($this->input->post('gender'));
        $high_school=trim($this->input->post('high_school'));
        $higher_education=trim($this->input->post('higher_education'));
        
        $current_location_country=trim($this->input->post('current_location_country'));
        $current_location_state=trim($this->input->post('current_location_state'));
        $current_location_city=trim($this->input->post('current_location_city'));
        
        $home_town_country=trim($this->input->post('home_town_country'));
        $home_town_state=trim($this->input->post('home_town_state'));
        $home_town_city=trim($this->input->post('home_town_city'));
        
        $traits=trim($this->input->post('traits'));
        $order=trim($this->input->post('order'));
        
        $sign="";
        if($sign_id!=""){
            $sign=$this->m_developer_sign->get_start_and_end_by_id($sign_id);
        }
        
        include_once(APPPATH.'controllers/class_file/c_search.php');
        $c_search_obj=new c_search();
        
	$c_search_obj->set_u_id($this->u_id);	
        $c_search_obj->set_name($name);
        $c_search_obj->set_age($age);
        $c_search_obj->set_sign($sign);
        $c_search_obj->set_gender($gender);
        $c_search_obj->set_high_school($high_school);
        $c_search_obj->set_higher_education($higher_education);
        
        $c_search_obj->set_home_town_country($home_town_country);
        $c_search_obj->set_home_town_state($home_town_state);
        $c_search_obj->set_home_town_city($home_town_city);
        
        $c_search_obj->set_current_location_country($current_location_country);
        $c_search_obj->set_current_location_state($current_location_state);
        $c_search_obj->set_current_location_city($current_location_city);
        
        $c_search_obj->set_traits($traits);
        $c_search_obj->set_order($order);
        $this->pageData['search_data']=$c_search_obj->get_advance_search_data();
        $this->load->view('search_result_view',$this->pageData);
    }
}

?>
