<?php
class custom_mail extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}
	function send_user_account_activation($to_email,$activation_key){
		$this->load->library('email');
		
//	$config['protocol'] = 'sendmail';
//		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['mailtype'] = 'html';
//		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);

		$this->email->from('infomation@frank.com', 'Admin');
		$this->email->to($to_email);
		//$this->email->reply_to('infomation@frank.com', 'Admin');
		$this->email->subject('Email Test');
		$this->email->message("To Activate Click Here<a href='".base_url()."activation?".
								"u_email=".$to_email.
								"&activation_key=".$activation_key."' >Activate</a></br>".
								"To Decline Click Here <a href='".base_url()."decline?".
								"u_email=".$to_email.
								"&activation_key=".$activation_key."' >Decline</a>");
		if(!$this->email->send()){
			echo $this->email->print_debugger();
		}
	}
}