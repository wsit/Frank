<?php
class c_frank_session extends CI_Controller {
	
	function __construct(){
		parent::__construct();
	}
	function create_login_session($email,$password){
		$this->load->model('m_user_login');
		$id=$this->m_user_login->get_id_by_email($email);
		
		$sessionData=array(
		    'id'=>$id,
			'email'=>$email,
		    'password'=>$password
		);
		$this->session->set_userdata('user_login_session', $sessionData);
	}
}