<?php
class c_information extends CI_Controller
{ 
	private $pageData;
	
	function __construct()
	{
		parent::__construct();
		$this->pageData=array();
		$this->load->helper('url');
		
	}
	function get_user_infomation_by_u_id($u_id)
	{
		if(!$u_id){
                    return false;
                }
                
		$this->load->model('m_user_login');
		$this->load->model('m_user_profile_data');
		$this->load->model('m_user_basic_info');
		$this->load->model('m_user_profile_settings');
		$this->load->model('m_developer_month');
		$this->load->model('m_developer_year');
		$this->load->model('m_developer_day');
		$this->load->model('m_developer_gender');
		$this->load->model('m_user_about_me');
		
		
		$this->pageData['user_all_gender']=$this->m_developer_gender->get_all();
		$this->pageData['user_profile_settings_all']=$this->m_user_profile_settings->get_all_by_u_id($u_id);
		$this->pageData['user_login_all']=$this->m_user_login->get_all_by_uid($u_id);
		$this->pageData['basic_info_all']=$this->m_user_basic_info->get_all_by_u_id($u_id);
		foreach ($this->pageData['basic_info_all'] as $rowData)
		{
			$rowData=explode("-",$rowData->birthday);
			
                    $year=$rowData[0];
		    $month=$rowData[1];
		    $day=$rowData[2];
			
		}
		$this->pageData['user_profile_data_all']=$this->m_user_profile_data->get_all_by_u_id($u_id);
		$this->pageData['date_name_all']=$this->m_developer_month->get_all_name();
		$this->pageData['month_data_all']=$this->m_developer_month->get_month_name_by_id($month);
		$this->pageData['year_name_all']=$this->m_developer_year->get_all_year();
		$this->pageData['year_name']=$this->m_developer_year->get_year_by_id($year);
		$this->pageData['day_name_all']=$this->m_developer_day->get_day_all();
		$this->pageData['day_name']=$this->m_developer_day->get_day_by_id($day);
		$this->pageData['user_about_all']=$this->m_user_about_me->get_all_by_u_id($u_id);
		
		return $this->pageData;
		
	}
       
}