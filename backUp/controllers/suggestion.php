<?php
class suggestion extends CI_Controller{
	
        function get_name(){
		$this->load->model('m_user_basic_info');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_basic_info->get_name_suggestion($term) as $rowData){
                    if($rowData!=""){	
                        array_push($keywords,$rowData->f_name.' '.$rowData->l_name);
                    }
		}
		echo json_encode($keywords);
	}
        function get_current_location_state(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_current_location_state_sugesstion($term) as $rowData){
                    if($rowData!=""){	
                        array_push($keywords,$rowData->current_location_1);
                    }
		}
		echo json_encode($keywords);
	}
        function get_current_location_city(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_current_location_city_sugesstion($term) as $rowData){
                    if($rowData!=""){	
                        array_push($keywords,$rowData->current_location_2);
                    }
		}
		echo json_encode($keywords);
	}
           function get_home_town_state(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_home_town_state_sugesstion($term) as $rowData){
                    if($rowData!=""){	
                        array_push($keywords,$rowData->home_town_1);
                    }
		}
		echo json_encode($keywords);
	}
        function get_home_town_city(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_home_town_city_sugesstion($term) as $rowData){
                    if($rowData!=""){	
                        array_push($keywords,$rowData->home_town_2);
                    }
		}
		echo json_encode($keywords);
	}
	function get_current_location_from_user_profile_data(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_current_location_suggestion($term) as $rowData){
                    if($rowData!=""){	
                        array_push($keywords,$rowData);
                    }
		}
		echo json_encode($keywords);
	}
	function get_home_town_suggestion_from_user_profile_data(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_home_town_suggestion($term) as $rowData){
			array_push($keywords,$rowData);
		}
		echo json_encode($keywords);
	}
	function get_organization_suggestion_from_user_profile_data(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_organization_suggestion($term) as $rowData){
			array_push($keywords,$rowData);
		}
		echo json_encode($keywords);
	}
	function get_high_school_suggestion_from_user_profile_data(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_high_school_suggestion($term) as $rowData){
			array_push($keywords,$rowData);
		}
		echo json_encode($keywords);
	}
	function get_higher_education_suggestion_from_user_profile_data(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_higher_education_suggestion($term) as $rowData){
			array_push($keywords,$rowData);
		}
		echo json_encode($keywords);
	}
	function get_workplace_suggestion_from_user_profile_data(){
		$this->load->model('m_user_profile_data');
		$term=trim($this->input->post('term'));
		
		if(strlen($term)<1){
			die();
		}
		
		$keywords = array();
		$i=0;
		foreach($this->m_user_profile_data->get_workplace_suggestion($term) as $rowData){
			array_push($keywords,$rowData);
		}
		echo json_encode($keywords);
	}
}