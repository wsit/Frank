<?php
class m_developer_gender extends CI_Model
{
	function get_all()
	{
		$this->db->select('*');
		$this->db->from('developer_gender');
		return $this->db->get()->result();
	}
}