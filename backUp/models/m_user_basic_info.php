<?php
class m_user_basic_info extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function insertRow($rowData){
		if($this->db->insert('user_basic_info',$rowData))
			return true;
		return false;
	}
	function updateRow_BY_id($id,$rowData){
		$this->db->where('u_id',$id);
		if($this->db->update('user_basic_info',$rowData))
				return true;
			return false;
	}
	function updateRow_BY_u_id($u_email,$rowData){
		$this->db->where('u_id',$u_email);
		if($this->db->update('user_basic_info',$rowData))
				return true;
			return false;
	}
	function get_all_by_u_id($u_id)
	{
		$this->db->select('*');
		$this->db->from('user_basic_info');
		$this->db->where('u_id',$u_id);
		return   $this->db->get()->result();
		
	}
	function get_gender_by_u_id($u_id){
		$this->db->select('gender');
		$this->db->from('user_basic_info');
		$this->db->where('u_id',$u_id);
		foreach($this->db->get()->result() as $rowData){
			return $rowData->gender;
		}
		return '';
	}
	function get_birthday_update_view_by_u_id($u_id){
		$this->db->select('birthday');
		$this->db->from('user_basic_info');
		$this->db->where('u_id',$u_id);
		foreach($this->db->get()->result() as $rowData){
			return $rowData->birthday;
		}
		return '';
	}
        function get_birthday_by_u_id($u_id){
		$this->db->select('user_basic_info.birthday as birth_day',false);
		$this->db->from('user_basic_info');
		$this->db->where('user_basic_info.u_id',$u_id);
		foreach($this->db->get()->result() as $rowData){
			return $rowData->birth_day;
		}
		return '';
	}
        function get_pic_path_by_u_id($u_id){
               $this->db->select('pic_path');
               $this->db->from('user_basic_info');
               $this->db->where('user_basic_info.u_id',$u_id);
               foreach($this->db->get()->result() as $rowData){
                       return $rowData->pic_path;
               }
               return '';
       }
        function get_name_by_u_id($u_id){
               $this->db->select('f_name,l_name');
               $this->db->from('user_basic_info');
               $this->db->where('user_basic_info.u_id',$u_id);
               foreach($this->db->get()->result() as $rowData){
                       return $rowData->f_name.' '.$rowData->l_name;
               }
               return '';
       }
       function get_name_suggestion($term){
               $this->db->distinct();
               $this->db->select('f_name,l_name');
               $this->db->from('user_basic_info');
               $this->db->like('f_name',$term,'match');
               $this->db->or_like('l_name',$term,'match');
               
               return $this->db->get()->result();
       }
}