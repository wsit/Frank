<?php

class m_search extends CI_Model{
    function get_search_data
    (
           $u_id,
           $name,
           $age,
           $sign,
           $gender,
           $high_school,
           $higher_education,
           $current_location_country,
           $current_location_state,
           $current_location_city,
           $home_town_country,
           $home_town_state,
           $home_town_city,
           $traits, 
           $order
           
     )
     {
       
        $this->db->distinct();
        $this->db->select('
                            basic_inf.f_name,
                            basic_inf.l_name,
                            basic_inf.pic_path,
                            FLOOR(ROUND(TO_DAYS(CURDATE()) - TO_DAYS(basic_inf.birthday) )/365) as d
                          ',false);
        $this->db->from('user_profile_data,user_basic_info basic_inf');
        $this->db->where('user_profile_data.u_id = basic_inf.u_id');
        $this->db->where('basic_inf.u_id !=',$u_id);
        if($name!=""){
             $this->db->like('basic_inf.f_name',$name,'match');
             $this->db->or_like('basic_inf.l_name',$name,'match');
        }
        if($age!=""){
             $this->db->where('FLOOR(ROUND(TO_DAYS(CURDATE()) - TO_DAYS(basic_inf.birthday) )/365) <= ',$age);
             $this->db->where('FLOOR(ROUND(TO_DAYS(CURDATE()) - TO_DAYS(basic_inf.birthday) )/365) >= ',$age);
            
        }
        
        if($sign!=""){
            list($s_date,$e_date)=  explode(" ", $sign);
            list($s_year,$s_month,$s_day)=  explode("-", $s_date);
            list($e_year,$e_month,$e_day)=  explode("-", $e_date);
            $this->db->where("DATE_FORMAT(birthday,'%m-%d') >=",$s_month.'-'.$s_day);
            $this->db->where("DATE_FORMAT(birthday,'%m-%d') <=",$e_month.'-'.$e_day);
           
        }
        if($gender!=""){
             $this->db->where('gender',$gender);
        }
        if($high_school!=""){
             $this->db->like('high_school',$high_school,'match');
        }
        if($higher_education!=""){
             $this->db->like('higher_education_1',$higher_education,'match');
             $this->db->or_like('higher_education_2',$higher_education,'match');
        }
        //if($current_location_country!=""){$this->db->like('',$current_location_country,'match'); }
        if($current_location_state!=""){$this->db->like('current_location_1',$current_location_state,'match'); }
        if($current_location_city!=""){ $this->db->like('current_location_2',$current_location_city,'match'); }
        //if($home_town_country!=""){ $this->db->like('',$home_town_country,'match'); }
        if($home_town_state!=""){ $this->db->like('home_town_1',$home_town_state,'match'); }
        if($home_town_city!=""){ $this->db->like('home_town_2',$home_town_city,'match'); }
        //if($traits!=""){ $this->db->like('',$traits,'match'); } 
        //if($order!=""){ $this->db->like('',$order,'match'); }
        
        //var_dump($this->db->get()->result());
        return $this->db->get()->result();
     }
}

?>
