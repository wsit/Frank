<?php
	 class c_user_visitor_details extends CI_Controller{
		 private $id;
		 private $visitor_id;
		 private $visitor_ip;
		 private $host_id;
		 private $created_date;
		 function __construct(){

			parent::__construct();

			$this->id='';
			$this->visitor_id='';
			$this->visitor_ip='';
			$this->host_id='';
			$this->created_date=date('Y-m-d H:i:s');
		 }
		 function set_id($id)
		 {
			$this->id=$id;
		 }

		 function set_visitor_id($visitor_id)
		 {
			$this->visitor_id=$visitor_id;
		 }

		 function set_visitor_ip($visitor_ip)
		 {
			$this->visitor_ip=$visitor_ip;
		 }

		 function set_host_id($host_id)
		 {
			$this->host_id=$host_id;
		 }

		 function set_created_date($created_date)
		 {
			$this->created_date=$created_date;
		 }

		 function insert_row_data(){
			$this->load->model('m_user_visitor_details');
                        $this->load->model('m_user_trait_final_values');
			$insert_array_data=array(
					 'visitor_id' => $this->visitor_id,
					 'visitor_ip' => $this->visitor_ip,
					 'host_id' => $this->host_id,
					 'created_date' => $this->created_date
					 );
                        $user_visitor_data_array=array();
                        $this->m_user_trait_final_values->increase_visitor($this->host_id);
                        $this->m_user_trait_final_values->increase_popularity($this->host_id,0.1);
                        
			return $this->m_user_visitor_details->insert_row_data($insert_array_data);
		 }
	 }
?>
