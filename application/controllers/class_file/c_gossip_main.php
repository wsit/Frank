<?php
	 class c_gossip_main extends CI_Controller{
		 private $id;
		 private $gossipper_id;
		 private $u_id;
		 private $thread_value;
		 private $view_count;
		 private $total_reply;
		 private $created_date;
		 function __construct(){

			parent::__construct();
                        $this->load->model('m_gossip_main');
                        include_once(APPPATH.'controllers/class_file/time_zone.php');
			$this->id='';
			$this->gossipper_id='';
			$this->target_id='';
			$this->thread_value='';
			$this->view_count='';
			$this->total_reply='';
			$this->created_date=date('Y-m-d H:i:S');
		 }
		 function set_id($id)
		 {
			$this->id=$id;
		 }

		 function set_gossipper_id($gossipper_id)
		 {
			$this->gossipper_id=$gossipper_id;
		 }

		 function set_target_id($u_id)
		 {
			$this->target_id=$u_id;
		 }

		 function set_thread_value($thread_value)
		 {
			$this->thread_value=$thread_value;
		 }

		 function set_view_count($view_count)
		 {
			$this->view_count=$view_count;
		 }

		 function set_total_reply($total_reply)
		 {
			$this->total_reply=$total_reply;
		 }
                 
                 
                 function set_created_date($created_date)
		 {
			$this->created_date=$created_date;
		 }

		 function insert_row_data(){
			

			$insert_array_data=array(
					 'gossipper_id' => $this->gossipper_id,
					 'target_id' => $this->target_id,
					 'thread_value' => $this->thread_value,
					 'view_count' => $this->view_count,
					 'total_reply' => $this->total_reply,
					 'created_date' => $this->created_date
					 );
			return $this->m_gossip_main->insert_row_data($insert_array_data);
		 }
                 function get_latest_gossip_id(){
                      return $this->m_gossip_main->get_last_id_by_category_id();
                 }
                 function get_current_id_for_make_gossip(){
                    
                     return $this->m_gossip_main->get_id_by_gossiper_id_target_id_thread_value(
                                                            $this->gossipper_id,
                                                            $this->target_id,
                                                            $this->thread_value);
                 }
                 function is_gossip_exist(){
                    
                     return $this->m_gossip_main->is_gossip_exist_by_gossiper_id_target_id_thread_value(
                                                            $this->gossipper_id,
                                                            $this->target_id,
                                                            $this->thread_value);
                 } 
                 function update_gossip_comment_id(){
                      $update_array=array('gossip_comment_id'=>$this->gossip_comment_id);
                      return $this->m_gossip_main->update_gossip_comment_id($update_array,$this->gossip_comment_id);
                 }
                function get_group_latest_gossip_id_by_members($user_list){
                    $user_list_str="(";
                    foreach ($user_list as $key=>$data){
                        $user_list_str.= $data['members_id'].',';
                    }
                    $user_list_str[strlen($user_list_str)-1]=")";
                    return $this->m_gossip_main->get_group_latest_gossip_id($user_list_str);
                }
                 function get_gossip_row_view_by_target_id($u_id,$from=false,$trait_categories_id="",$person_name="",$gossip_id="",$relation_ship="",$order=1){
                        $this->load->model('m_gossip_main');
                      
                        
                        $pageData=array();
                        $gossip_r_point=array();
                        $gossip_trait_subcategories=array();
                        $gossip_replies=array();
                        $gossip_target_user_trait=array();
                        $gossip_target_user_trait_point=array();
                        $gossiper_trait_point=array();
                        $pageData['gossip_like_equ']= '54+(($e_gossip_like - 1) * 4) + $e_gossip_like';
                        $pageData['gossip_for_him_count']=$this->m_gossip_main->get_total_gossip_count_by_target_id($u_id);
                        $pageData['gossip_from_him_count']=$this->m_gossip_main->get_total_gossip_count_by_gossipper_id($u_id);
                       
//                        if($trait_categories_id=="")
//                        {
//                           if($from){
//                               $trait_categories_id=$this->m_gossip_main->get_max_trait_category_id_by_gossiper_id($u_id);
//                           }else{
//                               $trait_categories_id=$this->m_gossip_main->get_max_trait_category_id_by_target_id($u_id);
//                           }
//                            if($trait_categories_id==""){
//                                return $pageData;
//                            }
//                           
//                        }
                        if($person_name!=""){
                            $person_id_array=$this->m_gossip_main->get_id_by_u_id($person_name);
                        }else{
                           
                            $person_id_array=array();
                        }
                         
                        if($gossip_id==""){
                            $gossip_rows_array=$this->m_gossip_main->get_j_gossip_row($u_id,$trait_categories_id,$person_id_array,$from,$order);
                        }else{
                             $gossip_rows_array=$this->m_gossip_main->get_j_gossip_row_by_gossip_id($gossip_id);
                        }
                        // Those are two dimention array index 0 represent value of gossiper and 1 for target_gossiper
                        foreach($gossip_rows_array as $rowData){
                            $gossip_r_point[$rowData->gossip_id][1]=$this->m_gossip_main->get_r_point_by_rated_by_and_gossip_id($rowData->gossip_id,$rowData->gossipper_id);
                            $gossip_trait_subcategories[$rowData->gossip_id][1]=$this->m_gossip_main->get_sub_category_by_id($trait_categories_id);
                            $gossip_replies[$rowData->gossip_id]=$this->m_gossip_main->get_row_count_by_gossip_id($rowData->gossip_id);
                            $gossip_target_user_trait[$rowData->gossip_id]=$this->m_gossip_main->get_j_gossip_target_trait_inf_by_gossip_id($rowData->gossip_id,$trait_categories_id);
                            foreach($gossip_target_user_trait[$rowData->gossip_id] as $trait_rowData){
                                $gossip_target_user_trait_point[$rowData->gossip_id][$trait_rowData->target_trait_categories_id]=$this->m_gossip_main->get_gossip_target_trait_point($rowData->gossip_id,$trait_rowData->target_trait_categories_id,0,$trait_rowData->target_id);
                                $gossiper_trait_point[$rowData->gossip_id][$trait_rowData->target_trait_categories_id]=$this->m_gossip_main->get_all_by_user_id_and_trait_categories_id($rowData->gossipper_id,$trait_rowData->target_trait_categories_id);
                            }
                        }
                        
                        if($from){
                            $pageData['gossip_from_rows']=$gossip_rows_array;
                            $pageData['gossip_from_r_point']=$gossip_r_point;
                            $pageData['gossip_from_trait_subcategories']=$gossip_trait_subcategories;
                            $pageData['gossip_from_replies']=$gossip_replies;
                            $pageData['gossip_from_target_id']=$u_id;
                            $pageData['gossip_from_target_user_trait']=$gossip_target_user_trait;
                            $pageData['gossip_from_target_user_trait_point']=$gossip_target_user_trait_point;
                            $pageData['gossip_from_gossiper_user_trait_point']=$gossiper_trait_point;
                        }else{
                            $pageData['gossip_rows']=$gossip_rows_array;
                            $pageData['gossip_r_point']=$gossip_r_point;
                            $pageData['gossip_trait_subcategories']=$gossip_trait_subcategories;
                            $pageData['gossip_replies']=$gossip_replies;
                            $pageData['gossip_target_id']=$u_id;
                            $pageData['gossip_target_user_trait']=$gossip_target_user_trait;
                            $pageData['gossip_target_user_trait_point']=$gossip_target_user_trait_point;
                            $pageData['gossip_gossiper_user_trait_point']=$gossiper_trait_point;
                        }
                       
                        return $pageData;
                 }
	 }
?>
