<?php
	 class gossip_relationship extends CI_Controller{
		 private $u_id;
                 private $gender;
                 private $pageData;
                 private $site_url;

                 private $sign_in;
                 private $sign_up;
                 private $sign_out;
                 private $search;
                 private $home;

                 function __construct(){
                    parent::__construct();
                    $this->load->helper('url');
                    $this->pageData=array();
                    include_once(APPPATH.'controllers/common_site_setting.php');
                    include_once(APPPATH.'controllers/load_common_properties_of_user_for_ajax_request_json.php');

                    $this->sign_in=FALSE;
                    $this->sign_up=FALSE;
                    $this->sign_out=TRUE;
                    $this->search=TRUE;
                    $this->home=FALSE;

                 }
		 function index(){
			
			 include_once(APPPATH.'controllers/class_file/c_gossip_relationship.php');
			 $c_gossip_relationship_obj=new c_gossip_relationship();

			//$id=trim($this->input->post('id'));
			//$gossip_id=trim($this->input->post('gossip_id'));
			//$values=trim($this->input->post('values'));
			//$point=trim($this->input->post('point'));
			//$created_by=trim($this->input->post('created_by'));
			//$created_date=trim($this->input->post('created_date'));

			
			$gossip_id='';
			$values='';
			$point='';
			$created_by='';
			

			
			$c_gossip_relationship_obj->set_gossip_id($gossip_id);
			$c_gossip_relationship_obj->set_values($values);
			$c_gossip_relationship_obj->set_point($point);
			$c_gossip_relationship_obj->set_created_by($created_by);
			

			if($c_gossip_relationship_obj->insert_row_data()){
				echo ';True;';
			}else{
				echo ';False;';
			}
		 }
                 function dispute(){
                     include_once(APPPATH.'controllers/class_file/encrypt_tbl_primary_key.php');
                        
                     $encrypt_tbl_primary_key_obj=new encrypt_tbl_primary_key();
                     $rel_id=""; 
                     $val="";
                     if(isset($_REQUEST['id'])){
                         $rel_id=$encrypt_tbl_primary_key_obj->get_plain_text($_REQUEST['id']);
                     }else{
                        $respond_array['status']="false";
                        $respond_array['msg']="Clean browser cache and Refreash Your page";
                        json_encode($respond_array);
                        echo json_encode($respond_array);
                        die();
                     }
                     
                     if(isset($_REQUEST['val'])){
                         $val=$_REQUEST['val'];
                     }else{
                        $respond_array['status']="false";
                        $respond_array['msg']="Clean browser cache and Refreash Your page";
                        json_encode($respond_array);
                        echo json_encode($respond_array);
                        die();
                     }
                     
                     $this->load->model('m_gossip_relationship');
                     $this->load->model('m_user_trait_final_values');
                     $updateData=array("accept"=>$val);
                     if($this->m_gossip_relationship->update_row_data($updateData,$rel_id)){
                        $respond_array['status']="true";
                        
                        $accepted_relation = $this->m_gossip_relationship->get_accepted_relation();
                        $disputed_relation = $this->m_gossip_relationship->get_disputed_relation();
                        /*
                         * In Document Formula is Sum of( Verfication point - Disputed Point )
                         * While we use minus[-] instead of plus[+]
                         * Because It getting the row where accpeted and disputed
                         * so Disputed row cout can't be negative
                         */
                        $total_verification_score =$accepted_relation + $disputed_relation; 
                        $profile_verification_score=(($accepted_relation - $disputed_relation)*100)/$total_verification_score;
                        $update_data_array['profile_verification']=$profile_verification_score;
                        $created_by= $this->m_gossip_relationship->get_created_by_by_gossip_id($rel_id);
                        $this->m_user_trait_final_values -> update_row_data($created_by,$update_data_array);
                       
                        if(intval($val)==0)
                            $respond_array['msg']="Disputed";
                        elseif(intval($val)==1)
                            $respond_array['msg']="Accepted";
                        
                        json_encode($respond_array);
                        echo json_encode($respond_array);
                        die();
                     }else{
                        $respond_array['status']="false";
                        $respond_array['msg']="Server Error";
                        json_encode($respond_array);
                        echo json_encode($respond_array);
                        die();
                     }
                    
                     
                 }
	 }
?>