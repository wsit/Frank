<?php
	 class trait_like extends CI_Controller{
                 private $u_id;
                 private $gender;
                 private $pageData;
                
                 function __construct(){
                    parent::__construct();
                    $this->load->helper('url');
                    $this->pageData=array();
                    include_once(APPPATH.'controllers/common_site_setting.php');
                    include_once(APPPATH.'controllers/load_common_properties_of_user_for_ajax_request_json.php');
                 }
		 function submit_insert_data(){
			
			 include_once(APPPATH.'controllers/class_file/c_trait_like.php');
                         include_once(APPPATH.'controllers/class_file/encrypt_tbl_primary_key.php');
                
                        $c_trait_like_obj=new c_trait_like();
			$encrypt_tbl_primary_key_obj=new encrypt_tbl_primary_key();
                        
			$gossip_id=$encrypt_tbl_primary_key_obj->get_plain_text(trim($this->input->post('gossip_id')));
			$comment_id=$encrypt_tbl_primary_key_obj->get_plain_text(trim($this->input->post('comment_id')));
			$main_comment=trim($this->input->post('main_comment'));
			$liker_id=$this->u_id;
			$gossiper_id=$encrypt_tbl_primary_key_obj->get_plain_text(trim($this->input->post('gossiper_id')));
			$point=  intval(trim($this->input->post('point')));
                        if($point>=-1 && $point<=1 ){
                           
                        }else{
                            $point=1;
                        }
			$c_trait_like_obj->set_gossip_id($gossip_id);
			$c_trait_like_obj->set_comment_id($comment_id);
			$c_trait_like_obj->set_main_comment($main_comment);
			$c_trait_like_obj->set_liker_id($liker_id);
			$c_trait_like_obj->set_gossiper_id($gossiper_id);
			$c_trait_like_obj->set_point($point);

			if($c_trait_like_obj->insert_row_and_update_gossip_main()){
				$respond_array['status']="true";
                                $respond_array['msg']="";
                                json_encode($respond_array);
                                echo json_encode($respond_array);
                                die();
			}else{
				$respond_array['status']="false";
                                $respond_array['msg']="";
                                json_encode($respond_array);
                                echo json_encode($respond_array);
                                die();
			}
		 }
	 }
?>