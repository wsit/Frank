<?php
class m_user_trait_final_values extends CI_Model{
	function insert_row_data($insert_data_array){
		return $this->db->insert('user_trait_final_values',$insert_data_array);
	}
	function update_row_data($user_id,$update_data_array){
		$this->db->where("user_id",$user_id);
		return $this->db->update('user_trait_final_values',$update_data_array);
	}
	function change_col_val($user_id,$sign,$col_name,$col_val){
		$sql="update user_trait_final_values set
				".$col_name." = ".$col_name." ".$sign." ".$col_val."
						where user_id = ".$user_id;
		return $this->db->query($sql);
		 
	}
	function increase_visitor($user_id){
		$sql="UPDATE user_trait_final_values SET visitor = visitor + 1 WHERE user_id = ".$user_id;
		$this->db->query($sql);
	}
	function increase_popularity($user_id,$point){
		$sql="UPDATE user_trait_final_values SET popularity = popularity + ".$point." WHERE user_id = ".$user_id;
		$this->db->query($sql);
	}
	function get_confidence_by_user_id($user_id){
		$this->db->select('confidence');
		$this->db->from('user_trait_final_values');
		$this->db->where("user_id",$user_id);
		$this->db->limit(1);
		foreach($this->db->get()->result() as $rowData){
			return $rowData->confidence;
		}
		return "";
	}
	function get_value_by_userid($user_id){
		$this->db->select('*');
		$this->db->from('user_trait_final_values');
		$this->db->where("user_id",$user_id);
		$this->db->limit(1);
		return $this->db->get()->result();
	}
	function get_top_rank_userinfo(){
		$sql = "SELECT  
                            ul.u_email,
                            ubi.pic_path,
                            upd.current_location_1,
                            ubi.birthday,
                            ubi.f_name,
                            ubi.l_name,
                            utfv.rank,
                            utfv.popularity  
				FROM `user_trait_final_values` AS utfv
                                INNER JOIN user_login AS ul ON  utfv.user_id = ul.id
				LEFT JOIN `user_basic_info` AS ubi ON utfv.`user_id` = ubi.`u_id`
				LEFT JOIN `user_profile_data` AS upd ON utfv.`user_id` = upd.`u_id`
				ORDER BY rank DESC
				LIMIT 1";
		
		$queryData = $this->db->query($sql);
		return $queryData->result();	
		
	}
	function get_top2_rank_userinfo(){
		$sql = "SELECT *
				FROM `user_trait_final_values` AS utfv
				LEFT JOIN `user_basic_info` AS ubi ON utfv.`user_id` = ubi.`u_id`
				LEFT JOIN `user_profile_data` AS upd ON utfv.`user_id` = upd.`u_id`
				ORDER BY rank DESC
				LIMIT 2";
		
		$queryData = $this->db->query($sql);
			
		if ($queryData->num_rows() > 0){
			return $queryData->result();
		} else {
			return false;
		}
	}
	
	function get_top3_populer_userinfo(){
		$sql = "SELECT *
				FROM `user_trait_final_values` AS utfv
				LEFT JOIN `user_basic_info` AS ubi ON utfv.`user_id` = ubi.`u_id`
				LEFT JOIN `user_profile_data` AS upd ON utfv.`user_id` = upd.`u_id`
				ORDER BY popularity DESC
				LIMIT 3";
		
		$queryData = $this->db->query($sql);
			
		if ($queryData->num_rows() > 0){
			return $queryData->result();
		} else {
			return false;
		}
	}
	
	function get_total_trait_final_values(){
		$sql = "SELECT ROUND(AVG(rank),0) AS total_rank, 
				ROUND(AVG(reputation),0) AS total_reputation, 
				ROUND(AVG(popularity),0) AS total_popularity,
				ROUND(AVG(integrity),0) AS total_integrity, 
				ROUND(AVG(confidence),0) AS total_confidence, 
				SUM(visitor) AS total_visitor
				FROM user_trait_final_values";
		
		$data = $this->db->query($sql);
		
		if($data->num_rows() >0 ){
			return $data->result();
		}else {
			return false;
		}
	}
}
