<?php

class m_search extends CI_Model{
    function get_search_data
    (
           $u_id,
           $name,
           $age,
           $sign,
           $gender,
           $high_school,
           $higher_education,
           $current_location_country,
           $current_location_state,
           $current_location_city,
           $home_town_country,
           $home_town_state,
           $home_town_city,
           $traits, 
           $order
           
     )
     {
       
        $this->db->distinct();
        $sql="  
                select
                    basic_inf.*,
                    user_login.*,
        			trait_categories.*,
        			trait_users.*,
        			user_profile_data.*,
                    FLOOR(ROUND(TO_DAYS(CURDATE()) - TO_DAYS(basic_inf.birthday) )/365) as d
                from   
                    user_profile_data,
                    user_login,
                    user_basic_info basic_inf,
        			trait_categories,
        			trait_users,
                    developer_activation
                 Where
                    (
                        user_profile_data.u_id = basic_inf.u_id AND
                        basic_inf.u_id !=".$u_id." AND
                        user_login.id = basic_inf.u_id AND
                        developer_activation.id = user_login.activation AND
                        trait_categories.id = trait_users.trait_categories_id AND
                        trait_users.user_id = user_profile_data.u_id AND
                        developer_activation.value = 'active'
                    )
                    
               ";
//        $this->db->where('user_profile_data.u_id = basic_inf.u_id');
//        $this->db->where('basic_inf.u_id !=',$u_id);
//        $this->db->where('user_login.id = basic_inf.u_id');
//        $this->db->where('developer_activation.id = user_login.activation');
//        $this->db->where('developer_activation.value',"active");
        if($name!=""){
           
            $sql.=" AND (";
            $sql.="         LOWER(basic_inf.f_name) like '%".strtolower($name)."%' OR ";
            $sql.="         LOWER(basic_inf.l_name) like '%".strtolower($name)."%' OR ";
            $sql.="         LOWER(CONCAT(basic_inf.f_name,' ',basic_inf.l_name)) like '%".strtolower($name)."%' OR ";
            $sql.="         CONCAT(basic_inf.l_name,' ',basic_inf.f_name) Like '%".$name."%'";
            $sql.="     )";
//             $this->db->like('basic_inf.f_name',$name,'match');
//             $this->db->or_like('basic_inf.l_name',$name,'match');
//             $this->db->or_like("CONCAT(basic_inf.f_name,' ',basic_inf.l_name)",$name,'match');
        }
        if($age!=""){
           
            $sql.=" AND (";
            $sql.="         FLOOR(ROUND(TO_DAYS(CURDATE()) - TO_DAYS(basic_inf.birthday) )/365) <= ".$age." AND ";
            $sql.="         FLOOR(ROUND(TO_DAYS(CURDATE()) - TO_DAYS(basic_inf.birthday) )/365) >= ".$age." ";
            $sql.="     )";
//             $this->db->where('FLOOR(ROUND(TO_DAYS(CURDATE()) - TO_DAYS(basic_inf.birthday) )/365) <= ',$age);
//             $this->db->where('FLOOR(ROUND(TO_DAYS(CURDATE()) - TO_DAYS(basic_inf.birthday) )/365) >= ',$age);
            
        }
        
        if($sign!=""){
          
            list($s_date,$e_date)=  explode(" ", $sign);
            list($s_year,$s_month,$s_day)=  explode("-", $s_date);
            list($e_year,$e_month,$e_day)=  explode("-", $e_date);
            if($this->is_capricorn($s_month,$s_day) || $this->is_capricorn($e_month,$e_day)){
                $sql.=" AND (";
                $sql.="     DATE_FORMAT(basic_inf.birthday,'%m-%d') >= '".$s_month."-".$s_day."' OR ";
                $sql.="     DATE_FORMAT(basic_inf.birthday,'%m-%d') <= '".$e_month."-".$e_day."'";
                $sql.="     )";
            }else{
                $sql.=" AND (";
                $sql.="     DATE_FORMAT(basic_inf.birthday,'%m-%d') >= '".$s_month."-".$s_day."' AND ";
                $sql.="     DATE_FORMAT(basic_inf.birthday,'%m-%d') <= '".$e_month."-".$e_day."'";
                $sql.="     )";
            }
//            $this->db->where("DATE_FORMAT(birthday,'%m-%d') >=",$s_month.'-'.$s_day);
//            $this->db->where("DATE_FORMAT(birthday,'%m-%d') <=",$e_month.'-'.$e_day);
           
        }
        if($gender!=""){
            
             $sql.=" AND (";
             $sql.="    basic_inf.gender = '".$gender."'";
             $sql.="     )";
        }
        if($high_school!=""){
            $sql.=" AND (";
            $sql.="    user_profile_data.high_school Like'%".$high_school."%'";
            $sql.="     )";
        }
        if($higher_education!=""){
             $sql.=" AND (";
             $sql.="    user_profile_data.higher_education_1 Like '%".$higher_education."%'  OR";
             $sql.="    user_profile_data.higher_education_2 Like '%".$higher_education."%'";
             $sql.="     )";
        }
        //if($current_location_country!=""){$this->db->like('',$current_location_country,'match'); }
        if($current_location_state!=""){
             $sql.=" AND (";
             $sql.="    user_profile_data.current_location_1 Like '%".$current_location_state."%' ";
             $sql.="     )";    
        }
        if($current_location_city!=""){ 
             $sql.=" AND (";
             $sql.="    user_profile_data.current_location_2 Like '%".$current_location_city."%' ";
             $sql.="     )";   
            }
        //if($home_town_country!=""){ $this->db->like('',$home_town_country,'match'); }
        if($home_town_state!=""){ 
             $sql.=" AND (";
             $sql.="    user_profile_data.home_town_1 Like '%".$home_town_state."%' ";
             $sql.="     )";  
        }
        if($home_town_city!=""){ 
             $sql.=" AND (";
             $sql.="    user_profile_data.home_town_2 Like '%".$home_town_city."%' ";
             $sql.="     )";  
         }
        if($traits!=""){
        	$sql .= " AND (";
        	$sql .= " trait_categories.id=".$traits;
        	$sql .= " ) ";
        } 
        //if($traits!=""){ $this->db->like('',$traits,'match'); } 
        //if($order!=""){ $this->db->like('',$order,'match'); }
        
        //var_dump($this->db->get()->result());
        
        $data =  $this->db->query($sql)->result();
        //echo $this->db->last_query();
        return $data;
     }
     
     function is_capricorn($m,$d){
         if(( $m==12 && $d>=22 ) || ($m==1 && $d<=20))
             return true;
         return fale;
     }    
     
     function get_group_top3_populer_by_userlist($userList){
     	$userlist = implode(',', $userList);
     	$sql = "SELECT *
				FROM user_login AS ul
				LEFT JOIN user_basic_info AS ubi ON ul.id= ubi.u_id
				LEFT JOIN user_trait_final_values AS utfv ON ul.id = utfv.user_id
				LEFT JOIN user_profile_data AS upd ON ul.id = upd.u_id
				WHERE ul.id IN ({$userlist})
				ORDER BY utfv.popularity DESC
				LIMIT 3";
     	
     	$data = $this->db->query($sql);
     	
     	if($data->num_rows() >0 ){
     		return $data->result();
     	}else {
     		return false;
     	}
     }
     
     function get_group_top2_finest_by_userlist($userList){
     	$userlist = implode(',', $userList);
     	$sql = "SELECT *
		     	FROM user_login AS ul
		     	LEFT JOIN user_basic_info AS ubi ON ul.id= ubi.u_id
		     	LEFT JOIN user_trait_final_values AS utfv ON ul.id = utfv.user_id
		     	LEFT JOIN user_profile_data AS upd ON ul.id = upd.u_id
		     	WHERE ul.id IN ({$userlist})
		     	ORDER BY utfv.rank DESC
		     	LIMIT 2";
     	
     	$data = $this->db->query($sql);
     	
     	if($data->num_rows() >0 ){
     	return $data->result();
     	}else {
     	return false;
     	}
     }
     
     function get_group_comparison_by_userlist_and_trait($userList, $traitID,$order='DESC'){
     	$userlist = implode(',', $userList);
     	$sql = "SELECT *
				FROM user_login AS ul
				LEFT JOIN user_basic_info AS ubi ON ul.`id`= ubi.`u_id`
				LEFT JOIN trait_users AS tu ON ul.`id` = tu.`user_id`
				LEFT JOIN trait_categories AS tc ON tu.`trait_categories_id` = tc.`id`
				LEFT JOIN user_profile_data AS upd ON ul.id = upd.`u_id`
				LEFT JOIN user_trait_final_values AS utfv ON ul.`id` = utfv.`user_id`
				WHERE ul.`id` IN ({$userlist})
				AND tu.`trait_categories_id`={$traitID}
				ORDER BY tu.`sub_category_avg_point` {$order} ";
     	
     	$data = $this->db->query($sql);
     	
     	if($data->num_rows() >0 ){
     		return $data->result();
     	}else {
     		return false;
     	}
     }
     
     function get_latest_member_from_userlist($userList){
     	$userid = max($userList);
     	$sql = "SELECT *
				FROM user_login AS ul
				LEFT JOIN user_basic_info AS ubi ON ul.`id`= ubi.`u_id`
				LEFT JOIN user_profile_data AS upd ON ul.id = upd.`u_id`
				LEFT JOIN user_trait_final_values AS utfv ON ul.`id` = utfv.`user_id`
				WHERE ul.`id` = {$userid}";
     	
     	$data = $this->db->query($sql);
     	
     	if($data->num_rows() >0 ){
     		return $data->result();
     	}else {
     		return false;
     	}
     }
     
     function get_highest_trait_member_from_userlist($userList,$trait_id=null){
     	$userlist = implode(',', $userList);
     	$sql = "SELECT *
				FROM user_login AS ul
				LEFT JOIN user_basic_info AS ubi ON ul.`id`= ubi.`u_id`
				LEFT JOIN trait_users AS tu ON ul.`id` = tu.`user_id`
				LEFT JOIN user_profile_data AS upd ON ul.id = upd.`u_id`
				LEFT JOIN user_trait_final_values AS utfv ON ul.`id` = utfv.`user_id`
				LEFT JOIN trait_categories AS tc ON tu.`trait_categories_id` = tc.`id`
				WHERE ul.`id` IN ({$userlist})";
     	if(isset($trait_id)) {
     		$sql .= " AND tu.trait_categories_id = {$trait_id} ";
     	}
		$sql .= "ORDER BY tu.`sub_category_avg_point` DESC 
				LIMIT 1";
     	
     	$data = $this->db->query($sql);
     	
     	if($data->num_rows() >0 ){
     		return $data->result();
     	}else {
     		return false;
     	}
     }
     
     function get_traitlist_avg_by_userlist($userList){
     	$this->load->model('m_trait_users');
     	$this->load->model('m_trait_categories');
     	
     	$traitList = $this->m_trait_categories->get_all_categories();
     	$finalResult = array();
     	if(!empty($traitList)){
     		foreach ($traitList as $key=>$data):
     			$finalResult[$data->category] = $this->m_trait_users->get_trait_avgpoint_by_userlist_and_category($data->category,$userList);
     		endforeach;
     		
     		return $finalResult;
     	}else {
     		return false;
     	}
     }
     
     function get_userlist_total_status($userList){
     	$userlist = implode(',', $userList);
     	$sql = "SELECT 
     			ROUND(AVG(rank),0) AS total_rank, 
     			ROUND(AVG(reputation),0) AS total_reputation, 
     			ROUND(AVG(popularity),0) AS total_popularity,
				ROUND(AVG(integrity),0) AS total_integrity, 
				ROUND(AVG(confidence),0) AS total_confidence, 
				SUM(visitor) AS total_visitor
				FROM user_trait_final_values
				WHERE user_id IN ({$userlist})";
     	
     	$data = $this->db->query($sql);
     	
     	if($data->num_rows() >0 ){
     		return $data->result();
     	}else {
     		return false;
     	}
     }
     
     function get_userinfo_by_search_name_form_userlist($userList, $name){
     	$userlist = implode(',', $userList);
     	$sql = "SELECT * FROM `user_basic_info` AS ubi
		     	LEFT JOIN `user_profile_data` AS upd ON ubi.`u_id`= upd.`u_id`
		     	LEFT JOIN trait_users AS tu ON ubi.`u_id`=tu.`user_id`
		     	LEFT JOIN user_trait_final_values AS utfv ON ubi.u_id = utfv.user_id
		     	WHERE (ubi.f_name LIKE '%{$name}%'
		     	OR ubi.l_name LIKE '%{$name}%')
		     	AND ubi.u_id IN ({$userlist})
		     	LIMIT 1";
     		
     	$queryData = $this->db->query($sql);
     
     	if ($queryData->num_rows() > 0){
     		return $queryData->result();
     	} else {
     		return false;
     	}
     }
}
?>