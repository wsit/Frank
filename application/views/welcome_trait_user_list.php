<!-- <pre><?php print_r($replyData);?></pre> -->
<?php 
if(!empty($replyData)):
	foreach ($replyData as $key=>$data):
?>
<div class="pe_container">
	<div class="pe_circle_purple">
		<p class="p_multi"><?php echo $key+1; ?></p>
	</div>
	<div class="progress_extended">
		<?php
		$userImage =  base_url().'images/user.png';
		if(!is_null($data->pic_path)) {
			$userImage = base_url().'uploads/'.$data->pic_path;
		} 
		?>
		<a href="javascript:show_login_form()"><img src="<?php echo $userImage; ?>" height="50" width="50" /></a>
		<div class="pe_details">
			<a href="javascript:show_login_form()" style="text-decoration: none;">
				<span class="pe_span_large"><?php echo $data->f_name.' '.$data->l_name; ?></span> 
			</a>
			<span class="pe_span_small"><?php echo $data->current_location_1; ?></span>
			<span class="pe_span_small"><?php echo date('m/d/Y',strtotime($data->birthday))?></span>
		</div>
		<div class="pe_progress_container">
			<div class="progressbar" style="float: right; margin: 5px 5px 0 0px;">
				<!--Violet-->
				<div class="bottom_bar"></div>
				<div class="i_bar_v"
					style="border-right: 1px solid #111; width: 70%;">
					<span class="inside-middle"></span>
				</div>
				<div class="top_inf">
					<span class="span_name">Rank</span><span class="span_score"><span
						class="span_score"><?php echo $data->rank; ?></span> </span>
				</div>
			</div>
			<div class="progressbar" style="float: right; margin: 5px 5px 0 0px;">
				<!--green-->
				<div class="bottom_bar"></div>
				<?php
					$avgPoint = $data->sub_category_avg_point;
					$w = abs($avgPoint).'%';
					if($avgPoint < 0) {
						$cname = 'i_bar_red';
					}else {
						$cname = $data->color_class;
					}
				?>
				<div class="<?php echo $cname; ?>" style="border-right: 1px solid #111; width: <?php echo $w; ?>;">
					<span class="inside-middle"></span>
				</div>
				<div class="top_inf">
					<span class="span_name"><?php echo $data->sub_category_value; ?></span><span class="span_score"><span
						class="span_score"><?php echo $data->sub_category_avg_point; ?></span> </span>
				</div>
			</div>
		</div>

	</div>
</div>
<?php 
endforeach;
endif;
?>