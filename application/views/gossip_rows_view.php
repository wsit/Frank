<?php if(isset($gossip_rows) || isset($gossip_from_rows) ){
        if(isset($now_gossip_from) && $now_gossip_from){
            if(isset($gossip_from_rows)){
                $local_gossip_replies=$gossip_from_replies;
                $local_gossip_r_point=$gossip_from_r_point;
                $local_gossip_trait_subcategories=$gossip_from_trait_subcategories;
                $gossip_data=$gossip_from_rows;
                $local_gossip_target_user_trait=$gossip_from_target_user_trait;
                $local_gossip_target_user_trait_point=$gossip_from_target_user_trait_point;
                $local_gossip_gossiper_user_trait_inf=$gossip_from_gossiper_user_trait_point;
            }else{
               $gossip_data=array();
            }
        }elseif(isset($gossip_rows)){
            $local_gossip_replies=$gossip_replies;
            $local_gossip_r_point=$gossip_r_point;
            $local_gossip_trait_subcategories=$gossip_trait_subcategories;
            $gossip_data=$gossip_rows;
            $local_gossip_target_user_trait=$gossip_target_user_trait;
            $local_gossip_target_user_trait_point=$gossip_target_user_trait_point;
            $local_gossip_gossiper_user_trait_inf=$gossip_gossiper_user_trait_point;
        }else{
            $gossip_data=array();
        }
    }else{
        $gossip_data=array();
    }
    
?>
  <?php $noGossip=true; ?>
  <?php foreach ($gossip_data as $rowData){ ?>
  <?php if($noGossip){ $noGossip=false; } ?>
  <script type="text/javascript">
    // $(document).ready(function(){
    //   $('.scrollbar_large').tinyscrollbar(); 
    // });
  </script>
    <div class="dummy_gfm_goss gfm_extended">
                          <div class="gossiper_comm_ex">
                            <span class="goss_arrow"></span>
                              <div class="spec_circle_purple" style="margin:45px 0 0 -10px;">
                                <?php
                                    $gossip_date=new DateTime($rowData->gossip_date);
                                ?>
                                  <p class="round_span_small"><?php echo $gossip_date->format('m/d/y'); ?></p>
                                <p class="round_span_small"><?php echo $gossip_date->format('h:i a'); ?></p>
                              </div>
                              <div class="goss_teller goss_shadow_fix">
                                    <a href="<?php echo base_url()."/profile?email=".$rowData->gossiper_email; ?>">
                                        <?php if($rowData->gossiper_profile_picture!=""){ ?>
                                              <img src="<?php echo base_url().'uploads/'.$rowData->gossiper_profile_picture; ?>" >
                                        <?php }else{ ?>
                                              <img src="<?php echo base_url().'images/user.png'; ?>" >
                                        <?php } ?>
                                    </a>
                                    <div class="goss_tell_inf">
                                      <p><?php echo $rowData->gossiper_name; ?></p>
                                      <p class="goss_tell_span_fix"><?php echo $rowData->gossiper_location; ?></p><p class="goss_tell_span_fix">&nbsp;<?php $gossiper_birthday=new DateTime($rowData->gossiper_birthday); echo $gossiper_birthday->format("m/d/y"); ?></p>
                                    </div>
                                    <div class="goss_tell_prog">
                                    <?php foreach($local_gossip_target_user_trait[$rowData->gossip_id] as $trait_user_rowData){ ?>
                                        <?php foreach($local_gossip_gossiper_user_trait_inf[$rowData->gossip_id][$trait_user_rowData->target_trait_categories_id] as $gossiper_trait_inf_rowData){ ?>
                                               
                                                <div class="progressbar" ><!--Green-->
                                                  <div class="bottom_bar"></div>
                                                  <?php 
                                                        $trait_point_int=intval($gossiper_trait_inf_rowData->sub_category_avg_point);
                                                        if($trait_point_int>=0){ 
                                                            $i_bar_class_name=$gossiper_trait_inf_rowData->color_class;
                                                        }else{ 
                                                            $i_bar_class_name="i_bar_red";
                                                        }
                                                        $trait_point_str=(string)abs(intval($gossiper_trait_inf_rowData->sub_category_avg_point));
                                                   ?>
                                                  <div class="<?php echo $i_bar_class_name; ?>" style="border-right:1px solid #111;width:<?php echo $trait_point_str.'%'; ?>; ">
                                                      <span class="inside-middle"></span>
                                                  </div>
                                                  <div class="top_inf">
                                                      <span class="span_name"><?php echo $gossiper_trait_inf_rowData->sub_category_value; ?></span>
                                                      <span class="span_score">
                                                          <span class="span_score">
                                                              <?php echo $trait_point_int; ?>
                                                          </span>
                                                      </span>
                                                  </div>
                                              </div>
                                        <?php } ?>
                                         <?php break; ?>
                                    <?php } ?>
                                        <div class="progressbar"><!--violet-->
                                            <div class="bottom_bar"></div>
                                            <div class="i_bar_v" style="border-right:1px solid #111;width:75%"><span class="inside-middle"></span></div>
                                            <div class="top_inf"><span class="span_name">Rank</span><span class="span_score"><span class="span_score"><?php echo $rowData->gossiper_rank; ?></span></span></div>
                                      </div> 
                                    </div>
                              </div>
                              <div class="gossip_replier" style="display:block;">
                                  
                              </div>
                              
                          </div>
                          <div class="goss_topic_extended goss_shadow_fix">
                              <div class="goss_thread_container">  
                                <div class="gossip_thread">
                                  <span class="head_large_span" style="float:left;margin-top:4px;">Gossip Thread:</span>
                                  <div class="gossip_th_input" style="color:black;">&nbsp;<?php echo ( $rowData->gossip_hidden == 1 ? " ( Hidden by gossipee )" :$rowData->gossip_thread_value ); ?></div>
                                  <div class="goss_txt_large" style="float:left;color:black;display:none;">&nbsp;<?php echo ( $rowData->gossip_hidden == 1 ? " ( Hidden by gossipee )" :$rowData->gossip_comments ); ?></div>
                                 </div> 
                                  <div class="scrollbar_large">
                                    <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                                    <div class="viewport">
                                       <div class="overview" >
                                        &nbsp;<?php echo ( $rowData->gossip_hidden == 1 ? " ( Hidden by gossipee )" :$rowData->gossip_comments ); ?> 
                                        <!-- <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p> -->                  
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              <?php ( $rowData->gossip_hidden == 1 ? $goss_thread_hide_display="block" : $goss_thread_hide_display="none" ); ?>
                              <div class="goss_thread_hide" style="display : <?php echo $goss_thread_hide_display; ?>;" >
                                
                              </div>
                              <div style="width:25px;height:125px;float:left;">
                                <div class="gossip_for_me_hidden_content" >
                                    <input class="gossip_id" type="hidden" value="<?php echo $encrypt_tbl_primary_key_obj->get_encrypted_code($rowData->gossip_id); ?>" />
                                    <input class="gossip_comment_id" type="hidden" value="<?php echo $encrypt_tbl_primary_key_obj->get_encrypted_code($rowData->gossip_comments_id); ?>" />
                                    <input class="main_comment" type="hidden" value="1" />
                                    <input class="gossipper_id" type="hidden" value="<?php echo $encrypt_tbl_primary_key_obj->get_encrypted_code($rowData->gossipper_id); ?>" />
                                </div>
                                  <?php 
                                    $e_gossip_like=$rowData->gossip_like;
                                    $gossip_like_val=0;
                                    eval('$gossip_like_val = '.$gossip_like_equ.';');
                                  ?>
                                  
                              <div onclick="gossip_comment_like($(this))" style="cursor:pointer;">
                                    <img src="<?php echo base_url().'images/tup.png'; ?>" style="margin-top:2px;" >
                              </div>
                              <div class="progressbar_h">
                                  <div class="bottom_bar_h"></div>
                                  <div class="i_bar_h dummy_i_bar" style="border-right:2px solid #111;background:#00A650;height: <?php echo $gossip_like_val."%"; ?>"><span class="inside-middle_h"></span></div>
                                  <div class="top_info_h"></div>
                              </div> 
                              <div onclick="gossip_comment_dislike($(this))" style="cursor:pointer;"><img src="<?php echo base_url().'images/tdown.png'; ?>"></div>
                            </div> 
                              <div class="gossip_reply_topic" style="display:none;">
                                  
                              </div>
                              <!-- Small textarea container --> 
                               
                              <!-- Small textarea container --> 
                          </div>
                        <div class="reply_gossip_reply"></div>
                          <div class="goss_reply_ex">
                            <span class="goss_re_arrow"></span>
                              <div class="goss_tell_prog_con goss_shadow_fix">
                                <p>Gossip Scores</p>
                                 <?php $avg_progressbar_view_count=0; ?>
                                 <?php foreach($local_gossip_target_user_trait[$rowData->gossip_id] as $trait_user_rowData){ ?>
                                     <?php if($avg_progressbar_view_count==0){ 
                                            $progress_display="block";
                                        }else{ 
                                            $progress_display="none";
                                        } ?>
                                    <div class="progressbar" style="display:<?php echo $progress_display; ?>"><!--Green-->
                                          <div class="bottom_bar"></div>
                                          <?php 
                                                $trait_point_int=intval($local_gossip_target_user_trait_point[$rowData->gossip_id][$trait_user_rowData->target_trait_categories_id]);
                                                if($trait_point_int>=0){ 
                                                    $i_bar_class_name="i_bar_g";
                                                }else{ 
                                                    $i_bar_class_name="i_bar_red";
                                                }
                                                $trait_point_str=(string)abs(intval($local_gossip_target_user_trait_point[$rowData->gossip_id][$trait_user_rowData->target_trait_categories_id]));
                                           ?>
                                          <div class="<?php echo $i_bar_class_name; ?>" style="border-right:1px solid #111;width:<?php echo $trait_point_str.'%'; ?>; ">
                                              <span class="inside-middle"></span>
                                          </div>
                                          <div class="top_inf">
                                              <span class="span_name"><?php echo $trait_user_rowData->target_trait_sub_category_value; ?></span>
                                              <span class="span_score">
                                                  <span class="span_score">
                                                      <?php echo $trait_point_int; ?>
                                                  </span>
                                              </span>
                                          </div>
                                      </div>

                                  <?php if(intval($trait_user_rowData->target_avg_point)>=0){ 
                                                $i_bar_class_name="i_bar_v";
                                            }else{ 
                                                $i_bar_class_name="i_bar_red";
                                            }
                                            $avg_point_str=(string)abs(intval($trait_user_rowData->target_avg_point));

                                  ?>

                                <div class="progressbar" style="display:<?php echo $progress_display;?>"><!--violet-->
                                      <div class="bottom_bar"></div>
                                      <div class="<?php echo $i_bar_class_name; ?>" style="border-right:1px solid #111;width:<?php echo $avg_point_str."%"; ?>"><span class="inside-middle"></span></div>
                                      <div class="top_inf"><span class="span_name">Avg.Score</span><span class="span_score"><span class="span_score"><?php echo $trait_user_rowData->target_avg_point; ?></span></span></div>
                                    </div>

                                <?php $avg_progressbar_view_count++; ?>
                                <?php } ?>
                              </div> 
                                <div class="goss_tell_sp goss_shadow_fix">
                                    <a href="<?php echo base_url()."profile?email=".$rowData->target_email; ?>" >
                                      <?php if($rowData->target_profile_picture!=""){ ?>
                                        <img src="<?php echo base_url().'uploads/'.$rowData->target_profile_picture; ?>" >
                                      <?php }else{ ?>
                                           <img src="<?php echo base_url().'images/user.png'; ?>" >
                                      <?php } ?>
                                    </a>
                                </div>
                           </div>
         
       
        </div>
  <div class="other_content_div">
      <div class="reply_content_div"></div>
      <div class="gfm_quality" style="display:none;"></div>
      <div class="gfm_info" style="display:none;"></div>
  </div>
 
 <div class="gfm_info" >
        <div class="goss_tell_btn_div" style="width:920px;margin-top:2px;">            
            <div class="goss_tell_add">
                <?php if(isset($user_id)){ ?>
                <?php if($user_id!=$rowData->gossipper_id){ ?>
                    <div class="goss_tell_btn  dummy_add_f_btn">
                       <span><a href="javascript:void(0)" onclick="add_to_favourite(this,'<?php  echo $rowData->gossiper_email; ?>')">Add Favourites
                               <input type="hidden" value=<?php echo $encrypt_tbl_primary_key_obj->get_encrypted_code($rowData->gossipper_id); ?> />
                            </a></span>
                    </div>
                 <?php  } ?>
              
                <?php  if($user_id!=$rowData->gossipper_id){ ?>
                    <div class="goss_tell_btn" style="margin-left:10px;">
                        <span><a href="<?php echo base_url()."profile?email=".$rowData->gossiper_email."&action=new_gossip"; ?>">New gossip</a></span>
                    </div>
                <?php  } ?>
              <?php  }else{ ?>
                <div class="goss_tell_btn  dummy_add_f_btn">
                       <span><a href="javascript:void(0)" onclick="show_login_form()">Add Favourites</a></span>
                </div>
                <div class="goss_tell_btn" style="margin-left:10px;">
                        <span><a href="<?php echo base_url()."profile?email=".$rowData->gossiper_email."&action=new_gossip"; ?>">New gossip</a></span>
                </div>
              <?php } ?>
               
            </div>
            <div class="goss_tell_other goss_tell_o_fix">
                <div class="goss_tell_replies">
                      <span>Gossip replies:</span><span><?php echo $local_gossip_replies[$rowData->gossip_id]; ?></span>
                </div>
                 <?php if(isset($user_id)){ ?>
                    
                    <?php if(intval($local_gossip_replies[$rowData->gossip_id])>0){ ?>
                        <div class="goss_tell_btn" style="margin-left:100px;">
                          <span><a class="dummy_view_all_gossip_btn" href="javascript:void(0)" onclick="initiate_load_gossip_comment_view(this)">View all gossip</a></span>
                        </div>
                        <div class="goss_tell_btn" style="margin-left:10px;">
                                 <span><a class="dummy_reply_btn" href="javascript:void(0)" onclick="load_gossip_reply_view(this)" view_action="true" >Reply to all gossip</a></span>
                        </div>
                    <?php  }else{ ?>
                        <div class="goss_tell_btn" style="margin-left:100px;display:none;">
                          <span><a class="dummy_view_all_gossip_btn" href="javascript:void(0)" onclick="initiate_load_gossip_comment_view(this)">View all gossip</a></span>
                        </div>
                        <div class="goss_tell_btn" style="margin-left:203px;">
                                 <span><a class="dummy_reply_btn" href="javascript:void(0)" onclick="load_gossip_reply_view(this)" view_action="true" >Reply to all gossip</a></span>
                        </div>
                    <?php } ?>
                 <?php }else{ ?>
                        <div class="goss_tell_btn" style="margin-left:100px;">
                          <span><a class="dummy_view_all_gossip_btn" href="javascript:void(0)" onclick="show_login_form()">View all gossip</a></span>
                        </div>
                        <div class="goss_tell_btn" style="margin-left:10px;">
                          <span><a class="dummy_reply_btn" href="javascript:void(0)" onclick="show_login_form()" view_action="true" >Reply to all gossip</a></span>
                        </div>
                 <?php } ?>
            </div>
            
            <div class="goss_tell_add" style="width:196px;margin-left: -9px;" >
                 <?php if(isset($user_id)){ ?>
                    <?php if($user_id!=$rowData->target_id){ ?>
                        <div class="goss_tell_btn  dummy_add_f_btn">
                           <span><a href="javascript:void(0)" onclick="add_to_favourite(this,'<?php  echo $rowData->gossiper_email; ?>')">Add Favourites
                                   <input type="hidden" value=<?php echo $encrypt_tbl_primary_key_obj->get_encrypted_code($rowData->gossipper_id); ?>
                                </a></span>
                        </div>
                     <?php  } ?>

                    <?php  if($user_id!=$rowData->target_id){ ?>
                        <div class="goss_tell_btn" style="margin-left:10px;">
                            <span><a href="<?php echo base_url()."profile?email=".$rowData->target_email."&action=new_gossip"; ?>" >New gossip</a></span>
                        </div>
                    <?php  } ?>
                    <?php if($user_id==$rowData->target_id){
                            if(intval($rowData->gossip_hidden) == 0){  ?>
                                <div class="goss_tell_btn padding_fix">
                                  <span><a href="javascript:void(0)" onclick="gossip_privacy_action(this,1)" g_id="<?php echo $encrypt_tbl_primary_key_obj->get_encrypted_code($rowData->gossip_id); ?>">Hide Gossip</a></span>
                                </div>
                         <?php }elseif(intval($rowData->gossip_hidden) == 1){ ?>
                                <div class="goss_tell_btn padding_fix">
                                  <span><a href="javascript:void(0)" onclick="gossip_privacy_action(this,0)" g_id="<?php echo $encrypt_tbl_primary_key_obj->get_encrypted_code($rowData->gossip_id); ?>">Show Gossip</a></span>
                                </div>
                          <?php } ?>
                    <?php } ?>
                <?php }else{ ?>
                        <div class="goss_tell_btn  dummy_add_f_btn">
                            <span><a href="javascript:void(0)" onclick="show_login_form()">Add Favourites</a></span>
                        </div>
                        <div class="goss_tell_btn" style="margin-left:10px;">
                                <span><a href="<?php echo base_url()."profile?email=".$rowData->gossiper_email."&action=new_gossip"; ?>">New gossip</a></span>
                        </div>
                <?php } ?>
            </div>
           </div>
        </div>
    <?php } ?>
    <?php if($noGossip){ ?>
        <div class="empty_msg">Its your lucky day ! No Gossip for You ! </div>
    <?php } ?>
