<!-- 
<pre><?php //print_r($top2finest);?></pre>
-->
<?php include 'header.php' ?>

<script type="text/javascript" src="<?php echo base_url(); ?>jQuery/jquery.jqplot.min.js"></script>
<script	type="text/javascript" src="<?php echo base_url(); ?>jQuery/jqplot.highlighter.min.js"></script>
<script	type="text/javascript" src="<?php echo base_url();?>jQuery/jqplot.dateAxisRenderer.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/ddlevelsmenu-base.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/ddlevelsmenu-topbar.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/ddlevelsmenu-sidebar.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/graph.css" />

<script type="text/javascript" src="<?php echo base_url(); ?>jQuery/ddlevelsmenu.js">

<!-- <script  type="text/javascript" src="<?php echo base_url(); ?>js/graph_trait.js"></script>-->
<!--  By Tanveer -->
<script type="text/javascript">
// $(document).ready(function() {
//       // hides the slickbox as soon as the DOM is ready
//       $('.block1').animate({height: '116px'});
//   $('.block3').click(function(){
//   			$this = $(this);
//          $(this).parents().find('.block1').each(function(){
//                 $(this).attr("class");
//                 if($(this).css("height") == "116px") {
//                     $(this).animate({height: "100%"}, 500);
//                     $this.css('backgroundPosition', '1px -2px');
//                 } else {
//                     $(this).animate({height: "116px"}, 500);
//                     $this.css('backgroundPosition', '1px -27px');
//                     }
//                     return false;
//                  });
//           });
        
//         });
</script>
<script type="text/javascript">

$(document).ready(function() {
	/* Declrear Base Url */
	//BASE = '<?php //echo site_url(); ?>';
	//console.log
    // hides the slickbox as soon as the DOM is ready
    $('.block1').animate({height: '116px'});
	$('.block3').click(function(){
		$this = $(this);
         $(this).parents().find('.block1').each(function(){
         	$(this).attr("class");
            if($(this).css("height") == "116px") {
            	$(this).animate({height: "100%"}, 1000);
            	$this.css('backgroundPosition', '1px -2px');

			} else {
            	$(this).animate({height: "116px"}, 500);
            	$this.css('backgroundPosition', '1px -27px');
            	
            }
            	return false;
         });
    });
        
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
      $('.right_slider').animate({width:'15px'});
      $('.right_slider').css('backgroundPosition', '-188px 9px');
      $('.right_show').click(function(){
            if($('.right_slider').css("width") == "15px") {
                    $('.right_slider').animate({width: "200px"}, 300);
                     $('#appearence_right').show({width: "175px"},300);
                     $('.right_slider').css('backgroundPosition', '-4px 9px');

                } else {
                    $('.right_slider').animate({width: "15px"}, 300);
                     $('.right_show').show();
                     $('#appearence_right').hide({width: "175px"},300);
                     $('.right_slider').css('backgroundPosition', '-188px 9px');
                    }
                    return false;
                 });
  });
</script>
<!-- E of by tanveer -->
<script type="text/javascript">
	function authinticate(){
		var user_name=$('#user_name').attr('value');
		var password=$('#login_password').attr('value');
		if(user_name!="" && password!=""){
			$('#user_name').css('background','white');
			$('#login_password').css('background','white');
			$.ajax(
			           {
			            	type:"POST",
			            	data:{u_email:user_name,password:password},
			                url:$('#site_url').attr("value")+"login/authenticate",
			                success:function(data){
			        	   		var resp=data.split(";");
			        	   		if(resp[1]=="True"){
				        	   		window.location=$('#site_url').attr("value");
				        	   	}else if(resp[1]=="False"){
					        	   	$('#login_error_div').html(resp[2]);
					        	}
						    }
			     });
		}else{
			if(user_name==""){
				$('#user_name').css('background','red');
			}else{
				$('#password').css('background','red')
			}
		}
	}
	function show_login_form(){	
			$('#search_form_div').fadeOut(500,function(){
                                
				$('#login_form_div').fadeIn(500,function(){
                                    $('#user_name').focus();
                                });
                                
				$('#sign_in_btn_span').fadeOut(500,function(){
					$('#search_btn_span').fadeIn(500,function(){
                                           
                                        });
				});
			});
			
	}
	function show_search_form(){	
			$('#login_form_div').fadeOut(500,function(){
			$('#search_form_div').fadeIn(500,function(){});
			$('#search_btn_span').fadeOut(500,function(){
				$('#sign_in_btn_span').fadeIn(500,function(){
                                    
                                });
			});
	});

	}
  
</script>
<script type="text/javascript">
    function like(){
	var current_val=Math.round($('#like_val').height()/$('#like_val').parent().height() * 100);
	if(current_val<100){
		var c_height=$('#like_val').height();
		c_height++;
		$('#like_val').height(c_height);
		
	}
}
function dislike(){
	var current_val=Math.round($('#like_val').height()/$('#like_val').parent().height() * 100);
	if(current_val>0){
		var c_height=$('#like_val').height();
		c_height--;
		$('#like_val').height(c_height);
		
	}
}
$(function(){
	var timeout_like, clicker_like = $('#like_div');
	
	clicker_like.mousedown(function(){
	    timeout_like = setInterval(function(){
	        like();
	    },85);
	
	    return false;
	});
	
	$(document).mouseup(function(){
	    clearInterval(timeout_like);
	    return false;
	});
	var timeout_dislike, clicker_dislike = $('#dislike_div');
	
	clicker_dislike.mousedown(function(){
		timeout_dislike = setInterval(function(){
	        dislike();
	    },85);
	
	    return false;
	});
	
	$(document).mouseup(function(){
	    clearInterval(timeout_dislike);
	    return false;
	});
});
</script>
<body>
	<?php include 'advertise_left.php' ?>
	<?php include 'advertise_right.php' ?>
	<!-- Hidden Tag Don't Touch It Tanveer -->
	<input type="hidden" id="site_url" value="<?php echo $site_url;?>">
	<!-- Hidden Tag End -->
	<!--<div id="contentwrap">-->
	<div id="content">

		<div class="infoBar">
			<p>My City Profile</p>
		</div>
		<div class="top2_container">
			<div class="demobox_container">
				<div class="demobox" style="height: 200px; margin: 0px;">
					<span class="demobox_span"><?php echo (isset($groupName)) ? $groupName : $groupDemoName; ?></span>
					<div class="area1">
						<span><?php echo $total_member; ?> Members</span>
					</div>
					<div class="area1">
						<span><?php echo ($groupTotalGossip) ? $groupTotalGossip : 0; ?> Gossips</span>
					</div>
					<div class="area1">
						<span>0 Online</span>
					</div>
					<div class="area1">
						<span>#0 Group Ranked</span>
					</div>
				</div>
				<div class="goss_tell_btn favourites" style="margin: 5px 0 0 20px;">
					<a href="#" id="add_to_favourite">Add to favourites</a>
				</div>
				<!-- Bubble Speech -->
				<div id="create_group_popup" class="bubble_container" style="display:none;">
					<div class="bubble_speech">
						<input placeholder="Group Name" name="group_name" type="text" />
						<div class="quote_div">
							<button class="quote_btn quote_save">Save</button>
							<button class="quote_btn quote_cancel">Cancel</button>
						</div>
					</div>
					<div class="bubble_quote">
						<div class="quote"></div>
					</div>
				</div>
				<!-- Bubble Speech -->
			</div>
			<?php if(!empty($top2finest)): ?>
			<div class="top2_finest">
				<div class="round_div_purple_top">
					<p class="round_span">1</p>
				</div>
				<div class="top_finest_sub">
					<div class="heading">
						<span class="head_large_span"><?php echo $groupDemoName; ?>'s Finest</span>
					</div>
					<div class="personality">
					<?php if(isset($top2finest[0])): ?>
					<?php
						$userImage =  base_url().'images/user.png';
						if(isset($top2finest[0]->pic_path) && !empty($top2finest[0]->pic_path)) {
							$userImage = base_url().'uploads/'.$top2finest[0]->pic_path;
						} 
					?>
						<a style="text-decoration: none;" href="<?php echo base_url().'profile?email='.$top2finest[0]->u_email;?>">
							<img alt="Profile" src="<?php echo $userImage; ?>" height="100" width="70" />
						</a>
						<div class="personality_info">
							<?php $memberName = $top2finest[0]->f_name.' '.$top2finest[0]->l_name; ?>
							<a style="text-decoration: none;" href="<?php echo base_url().'profile?email='.$top2finest[0]->u_email;?>">
								<span title="<?php  echo $memberName; ?>" class="plarge_span"><?php echo str_truncate_words($memberName,8); ?></span>
							</a>
							<span class="psmall_span"><?php echo $top2finest[0]->current_location_1; ?></span> &nbsp; <span class="psmall_span"><?php echo date('m/d/Y',strtotime($top2finest[0]->birthday));?></span>
							<div class="personality_prog">
								<div class="progressbar">
									<!--Violet-->
									<div class="bottom_bar"></div>
									<div class="i_bar_v" style="border-right: 1px solid #111;width: 0%;">
										<span class="inside-middle"></span>
									</div>
									<div class="top_inf">
										<span class="span_name">Rank</span><span class="span_score"><span
											class="span_score"><?php echo $top2finest[0]->rank; ?></span> </span>
									</div>
								</div>
								<div class="progressbar">
									<!--Orange-->
									<div class="bottom_bar"></div>
									<div class="i_bar_o" style="border-right: 1px solid #111; width: 0%;">
										<span class="inside-middle"></span>
									</div>
									<div class="top_inf">
										<span class="span_name">Popularity</span>
										<span class="span_score">
											<span class="span_score"><?php echo $top2finest[0]->popularity; ?></span> 
										</span>
									</div>
								</div>
							</div>
						</div>
					<?php endif;?>
					</div>
				</div>
				<div class="specholder">
				<?php if(isset($top2finest[1])):?>
					<div class="spec_circle_red">
						<p class="p_red">2</p>
					</div>
					<!--style="background:#EF2E32;"-->
					<div class="specprogress">
					<?php
						$userImage =  base_url().'images/user.png';
						if(isset($top2finest[1]->pic_path) && !empty($top2finest[1]->pic_path)) {
							$userImage = base_url().'uploads/'.$top2finest[1]->pic_path;
						} 
					?>
						<a style="text-decoration: none;" href="<?php echo base_url().'profile?email='.$top2finest[1]->u_email;?>">
							<img src="<?php echo $userImage; ?>" alt="Profile" height="50" width="50" />
						</a>
						<div class="specp_div">
							<a style="text-decoration: none;" href="<?php echo base_url().'profile?email='.$top2finest[0]->u_email;?>">
								<span class="specp_span"><?php echo str_truncate_words($top2finest[1]->f_name.' '.$top2finest[1]->l_name,8); ?></span>
							</a>
							<span class="specp_small_span"><?php echo $top2finest[1]->current_location_1; ?></span>
						</div>
						<div class="specp_prog_div">
							<div class="progressbar"
								style="float: right; margin: 0 3px 3px 0px">
								<!--Violet-->
								<div class="bottom_bar"></div>
								<div class="i_bar_v" style="border-right: 1px solid #111; width: 0%;">
									<span class="inside-middle"></span>
								</div>
								<div class="top_inf">
									<span class="span_name">Rank</span>
									<span class="span_score">
										<span class="span_score"><?php echo $top2finest[0]->rank; ?></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				<?php endif;?>
				</div>
			</div>
			<?php endif; ?>
			
			<div class="top2_popular">
				<div class="heading" style="float: right; margin: 0 8px 0 0;">
					<span class="head_large_span"><?php echo $groupDemoName; ?>'s Most Popular</span>
				</div>
				<?php if(!empty($top3populer)): ?>
				<?php foreach ($top3populer as $key=>$userData):?>
				<?php 
					$userImage =  base_url().'images/user.png';
					if(isset($userData->pic_path) && !empty($userData->pic_path)) {
						$userImage = base_url().'uploads/'.$userData->pic_path;
					}
				?>
				<div class="specholder">
					<div class="spec_circle_purple">
						<!--style="background:#5E2590;"-->
						<p class="p_purple"><?php echo $key+1 ?></p>
					</div>
					<div class="specprogress">
						<a href="<?php echo base_url().'profile?email='.$userData->u_email;?>">
							<img src="<?php echo $userImage; ?>" height="50" width="50" />
						</a>
						<div class="specp_div">
							<?php $memberName = $userData->f_name.' '.$userData->l_name;?>
							<a href="<?php echo base_url().'profile?email='.$userData->u_email;?>">
								<span class="specp_span"><?php echo str_truncate_words($memberName,10); ?></span>
							</a>
							<span class="specp_small_span"><?php echo $userData->current_location_1; ?></span>
						</div>
						<div class="specp_prog_div">
							<div class="progressbar"
								style="float: right; margin: 0 3px 3px 0px">
								<!--Violet-->
								<div class="bottom_bar"></div>
								<div class="i_bar_o" style="border-right: 1px solid #111; width: 0%;">
									<span class="inside-middle"></span>
								</div>
								<div class="top_inf">
									<span class="span_name">Populerity</span>
									<span class="span_score">
										<span class="span_score"><?php echo $userData->popularity; ?></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach;?>
				<?php endif;?>
			</div>
			<div class="demobox2">
				<span>Stats</span><br>
				<?php 
					$w = 0;
					$avgPoint = $groupTotalstatus[0]->total_integrity;
					$w = abs($avgPoint).'%';
					if($avgPoint < 0) {
						$cname = 'i_bar_red';
					}else {
						$cname = 'i_bar_s';
					}
				?>
				<div class="progressbar">
					<!--Blue-->
					<div class="bottom_bar"></div>
					<div class="<?php echo $cname;?>" style="border-right: 1px solid #111; width: <?php echo $w;?>">
						<span class="inside-middle"></span>
					</div>
					<div class="top_inf">
						<span class="span_name">Integrity</span>
						<span class="span_score">
							<span class="span_score"><?php if(!empty($groupTotalstatus)){ echo $groupTotalstatus[0]->total_integrity; }?></span>
						</span>
					</div>
				</div>
				<?php 
					$w = 0;
					$avgPoint = $groupTotalstatus[0]->total_confidence;
					$w = abs($avgPoint).'%';
					if($avgPoint < 0) {
						$cname = 'i_bar_red';
					}else {
						$cname = 'i_bar_s';
					}
				?>
				<div class="progressbar">
					<!--Blue-->
					<div class="bottom_bar"></div>
					<div class="<?php echo $cname;?>" style="border-right: 1px solid #111; width: <?php echo $w;?>">
						<span class="inside-middle"></span>
					</div>
					<div class="top_inf">
						<span class="span_name">Confidence</span>
						<span class="span_score">
							<span class="span_score"><?php if(!empty($groupTotalstatus)){ echo $groupTotalstatus[0]->total_confidence; }?></span> 
						</span>
					</div>
				</div>
				<div class="progressbar">
					<!--Blue-->
					<div class="bottom_bar"></div>
					<div class="i_bar_s" style="border-right: 1px solid #111; width: 0%;">
						<span class="inside-middle"></span>
					</div>
					<div class="top_inf">
						<span class="span_name">Gossip for</span><span class="span_score">
						<span class="span_score"><?php echo (!empty($groupTotalGossipfor)) ? $groupTotalGossipfor : 0;?></span> </span>
					</div>
				</div>
				<div class="progressbar">
					<!--Blue-->
					<div class="bottom_bar"></div>
					<div class="i_bar_s" style="border-right: 1px solid #111; width: 0%;">
						<span class="inside-middle"></span>
					</div>
					<div class="top_inf">
						<span class="span_name">Gossip from</span><span class="span_score"><span
							class="span_score"><?php echo (!empty($groupTotalGossipfrom)) ? $groupTotalGossipfrom : 0;?></span> </span>
					</div>
				</div>
				<div class="progressbar">
					<!--Blue-->
					<div class="bottom_bar"></div>
					<div class="i_bar_s" style="border-right: 1px solid #111;width: 0%;">
						<span class="inside-middle"></span>
					</div>
					<div class="top_inf">
						<span class="span_name">Visitors</span>
						<span class="span_score">
							<span class="span_score"><?php if(!empty($groupTotalstatus)){ echo $groupTotalstatus[0]->total_visitor; }?></span>
						</span>
					</div>
				</div>
			</div>
		</div>

		<div class="top3_container" style="overflow: visible;">
			<div class="quality">
				<span class="ag_span">Aggrigates</span><br>
				<div class="round_div_violet">
					<p class="round_diff">84%</p>
				</div>				
				<div class="progressbar_l" style="background: #fff;">
					<!--violet-->
					<div class="bottom_bar_l"></div>
					<div id="t" class="i_bar_l_v"
						style="border-right: 1px solid #111; width: 0%;">
						<span class="inside-middle"></span>
					</div>
					<div class="top_inf_l">
						<span class="span_name">Rank Score</span><span class="span_score"><span
							class="span_score"><?php if(!empty($groupTotalstatus)){ echo $groupTotalstatus[0]->total_rank; }?></span> </span>
					</div>
				</div>
				<br>
				<div class="round_div_purple">
					<p class="round_diff">70%</p>
				</div>
				<?php 
					$w = 0;
					$avgPoint = $groupTotalstatus[0]->total_reputation;
					$w = abs($avgPoint).'%';
					if($avgPoint < 0) {
						$cname = 'i_bar_red';
					}else {
						$cname = 'i_bar_l_p';
					}
				?>
				<div class="progressbar_l" style="background: #fff;">
					<!--pink-->
					<div class="bottom_bar_l"></div>
					<div id="t" class="<?php echo $cname; ?>" style="border-right: 1px solid #111; width: 0%;">
						<span class="inside-middle"></span>
					</div>
					<div class="top_inf_l">
						<span class="span_name">Reputation</span><span class="span_score"><span
							class="span_score"><?php if(!empty($groupTotalstatus)){ echo $groupTotalstatus[0]->total_reputation; }?></span> </span>
					</div>
				</div>
				<br>
				<div class="round_div_orange">
					<p class="round_diff">75%</p>
				</div>
				<?php 
					$w = 0;
					$avgPoint = $groupTotalstatus[0]->total_reputation;
					$w = abs($avgPoint).'%';
					if($avgPoint < 0) {
						$cname = 'i_bar_red';
					}else {
						$cname = 'i_bar_l_o';
					}
				?>
				<div class="progressbar_l" style="background: #fff;">
					<!--orange-->
					<div class="bottom_bar_l"></div>
					<div id="t" class="<?php echo $cname; ?>" style="border-right: 1px solid #111; width: 0%;">
						<span class="inside-middle"></span>
					</div>
					<div class="top_inf_l">
						<span class="span_name">Popularity</span><span class="span_score"><span
							class="span_score"><?php if(!empty($groupTotalstatus)){ echo round($groupTotalstatus[0]->total_popularity,1); }?></span> </span>
					</div>
				</div>
			</div>
			<?php if(!empty($groupTraitListAvg)): $i = 0; ?>
			<?php foreach ($groupTraitListAvg as $key=>$traitInfo):?>
			<?php if($i > 3) :?>
				<div class="right_slider">
			<?php endif;?>
			<div class="quality" <?php if($i > 3){?> id="appearence_right" style="margin-left: 4px; display: none;"<?php }?>>
				<span class="tr_span"><?php echo $key; ?></span><br>
				<div class="block1" style="width: 160px;">
					<?php if(!empty($traitInfo)): $j = count($traitInfo);?>
					<?php foreach ($traitInfo as $subkey => $data): ?>
					<?php 
						$w = 0;
						$avgPoint = $data->total_trait;
						$w = abs($avgPoint).'%';
						if($avgPoint < 0) {
							$cname = 'i_bar_red';
						}else {
							$cname = $data->color_class;
						}
					?>
					<div class="hide_container">
						<div class="small_circle" style="margin: 5px 0 0 5px;display:none;">
							<span><a href="#">H</a> </span>
						</div>
						<div class="small_fake" style="margin: 5px 0 0 5px;"></div>
						<div class="progressbar" style="margin: 4px 0 2px 0px; float: left;">
							<!--green-->
							<div class="bottom_bar"></div>
							<div class="<?php echo $cname?>" style="border-right: 1px solid #111; width: <?php echo $w; ?>;">
								<span class="inside-middle"></span>
							</div>
							<div class="top_inf">
								<span class="span_name"><?php echo $data->sub_category; ?></span>
								<span class="span_score">
									<span class="span_score"><?php echo $avgPoint; ?></span> 
								</span>
							</div>
						</div>
					</div>					
					<?php endforeach;?>
					<?php endif;?>				
				</div>
				<?php if($j > 4):?>
				<div class="block3" id="toggle1">
					<img id="show_img" class="show"	style="margin: 0 auto; display: none;" src="<?php echo base_url(); ?>images/downarw.png" height="25" width="25"> 
					<img id="show_img_up" class="show" style="margin: 0 auto; display: none;" src="<?php echo base_url(); ?>images/uparrow.png" height="25" width="25">
				</div>
				<?php endif;?>
			</div>
			<?php if($i > 3) :?>
				<img class="right_show" style="float: right;" src="<?php echo base_url(); ?>images/side_arrow.png" height="15" width="15">
			</div>
			<?php endif;?>			
			<?php $i++; endforeach;?>
			<?php endif;?>			
		</div>
		<!-- static -->
		<div class="middle_fake">
			<p>Member Comparison (<?php echo $total_member;?>)</p>
		</div>
		<div class="middle_container">
			<div class="select_area">
				<form id="group_form" name="group_form">
					<div class="selectbox1">
						<span class="sbox_span">Traits</span><br>
						<?php $selectedTrait = (isset($selected_trait)) ? $selected_trait : $category_tree[key($category_tree)][0]['id'];?> 						
						<input type="hidden" name="sub_trait" value="<?php echo $encryptObj->get_encrypted_code($selectedTrait);?>">
						<div id="ddtopmenubar" class="mattblackmenu">
							<ul>
								<li><?php if(!empty($category_tree)):?>
								<a id="selectedView" onclick="return false;" href="#" rel="ddsubmenu1"><?php echo $category_tree[key($category_tree)][0]['sub_category'];?></a>
									<ul id="ddsubmenu1" class="ddsubmenustyle">
										<?php foreach ($category_tree as $key=>$data):?>
										<li><a href="#" onclick="return false;"><?php echo $key;?> </a> <?php if(is_array($data)):?>
											<ul>
												<?php foreach ($data as $skey=>$subdata):?>
												<li><a href="<?php echo $encryptObj->get_encrypted_code($subdata['id']);?>" class="selectTrait" ><?php echo $subdata['sub_category'];?> </a>
												</li>
												<?php endforeach;?>
											</ul> <?php endif;?>
										</li>
										<?php endforeach;?>
									</ul> <?php endif;?>
								</li>
							</ul>
						</div>
						<script type="text/javascript">
							ddlevelsmenu.setup("ddtopmenubar", "topbar"); //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
						 </script>
					</div>
					<div class="selectbox2">
						<span class="sbox_span">Order</span><br> 
						<select name="order">
							<option value="1">Highest</option>
							<option value="0">Lowest</option>
						</select>
					</div>
					<!-- <a href="#" target="_blank"><div class="btn_div">Submit</div> </a> -->
					<button class="btn_div btn_div_ex">Submit</button>
				</form>
				<form id="group_person_select_form" name="group_person_select_form" action="#">	
					<div class="selectbox3">
						<span class="sbox_span">Select Person</span><br> 
						<input type="text" name="search_name" placeholder="Person Search">
					</div>
					<button type="submit" class="btn_div btn_div_ex">Submit</button>
				</form>	
			</div>
			<div class="middle_left">				
				<?php if(!empty($userTraitInfo)): ?>
				<?php foreach ($userTraitInfo as $key=>$userData):?>
				<?php 
					$userImage =  base_url().'images/user.png';
					if(isset($userData->pic_path) && !empty($userData->pic_path)) {
						$userImage = base_url().'uploads/'.$userData->pic_path;
					}
				?>
				<div class="pe_container">
					<input type="hidden" name="user_id" value="<?php echo $encryptObj->get_encrypted_code($userData->u_id); ?>">
					<div class="pe_circle_purple">
						<p class="p_multi"><?php echo $key+1; ?></p>
					</div>
					<div class="progress_extended">
						<a href="<?php echo base_url().'profile?email='.$userData->u_email;?>">
							<img src="<?php echo $userImage; ?>" height="50" width="50" />
						</a>
						<div class="pe_details">
							<a href="<?php echo base_url().'profile?email='.$userData->u_email;?>">
								<span class="pe_span_large"><?php echo str_truncate_words($userData->f_name.' '.$userData->l_name, 14)?></span>
							</a>
							<span class="pe_span_small"><?php echo $userData->current_location_1; ?></span>
							<span class="pe_span_small"><?php echo date('m/d/Y',strtotime($userData->birthday));?></span>
						</div>
						<div class="pe_progress_container">
							<div class="progressbar"
								style="float: right; margin: 5px 5px 0 0px;">
								<!--Violet-->
								<div class="bottom_bar"></div>
								<div class="i_bar_v"
									style="border-right: 1px solid #111; width: 0%;">
									<span class="inside-middle"></span>
								</div>
								<div class="top_inf">
									<span class="span_name">Rank</span><span class="span_score"><span
										class="span_score"><?php echo $userData->rank; ?></span> </span>
								</div>
							</div>
							<?php
								$w = 0;
								$avgPoint = $userData->sub_category_avg_point;
								$w = abs($avgPoint).'%';
								if($avgPoint < 0) {
									$cname = 'i_bar_red';
								}else {
									$cname = $userData->color_class;
								} 
							?>
							<div class="progressbar" style="float: right; margin: 5px 5px 0 0px;">
								<!--green-->
								<div class="bottom_bar"></div>
								<div class="<?php echo $cname; ?>" style="border-right: 1px solid #111; width: <?php echo $w; ?>;">
									<span class="inside-middle"></span>
								</div>								
								<div class="top_inf">
									<span class="span_name"><?php echo $userData->sub_category_value; ?></span>
									<span class="span_score"><span class="span_score"><?php echo $avgPoint; ?></span> </span>
								</div>
							</div>
						</div>
						<div class="pe_button_container">
							<button class="pe_btn" style="margin-left: 40px;">
								<span>Add Favourites</span>
							</button>
							<button class="pe_btn">
								<span>New Gossip</span>
							</button>
						</div>
					</div>
				</div>
				<?php endforeach;?>
				<?php endif;?>
				
			</div>
			<div class="middle_right">
				<div id="searched_user" class="progress_extended" style="margin-left: 20px; display: none;">
				<!-- searched user info -->
				</div>
				
				<div class="graph_cont" id="graph_distribute">
					<h1>Distribution of score</h1>
					<div class="graph">
						<canvas id="g1" width="370" height="190" style="width: 100%; height: 100%; float: left; position: absolute; top: 0; left: 0;"></canvas>
					</div>
					<div class="y_axis">
						<div>100%</div>
						<div>50%</div>
						<div>0%</div>
					</div>
					<div class="x_axis g1">
						<div>-100</div>
						<div>-90</div>
						<div>-80</div>
						<div>-70</div>
						<div style="margin-right: 1px;">-60</div>
						<div style="margin-right: 1px;">-50</div>
						<div>-40</div>
						<div style="margin-right: 1px;">-30</div>
						<div>-20</div>
						<div style="margin-right: 1px;">-10</div>
						<div>0</div>
						<div>10</div>
						<div>20</div>
						<div>30</div>
						<div style="margin-right: 1px;">40</div>
						<div style="margin-right: 1px;">50</div>
						<div style="margin-right: 1px;">60</div>
						<div>70</div>
						<div>80</div>
						<div style="margin-right: 2px;">90</div>
						<div>100</div>
					</div>
				</div>
				<div class="graph_cont">
					<h1>Distribution of score</h1>
					<div class="graph">
						<canvas id="g2" width="370" height="190"
							style="width: 100%; height: 100%; float: left; position: relative;"></canvas>
					</div>
					<div class="y_axis">
						<div>100</div>
						<div>0</div>
						<div>-100</div>
					</div>
					<div class="x_axis g2">
						<div style="margin-left: 10px;">12/10</div>
						<div>1/11</div>
						<div style="margin-left: 1px;">2/11</div>
						<div style="margin-left: 1px;">3/11</div>
						<div style="margin-left: 1px;">4/11</div>
						<div style="margin-left: 1px;">5/11</div>
						<div style="margin-left: 1px;">6/11</div>
						<div style="margin-left: 1px;">7/11</div>
						<div style="margin-left: 1px;">8/11</div>
						<div>9/11</div>
						<div style="margin-left: 1px;">10/11</div>
						<div>11/11</div>
					</div>
				</div>
			</div>
		</div>

		<div class="info_wrapper">
			<div class="latest_member">
				<?php if(!empty($latestMember)):?>
				<div class="round_div_purple_top">
					<p class="round_span_small"><?php echo date('m/d/Y',strtotime($latestMember[0]->c_date));?></p>
					<p class="round_span_small"><?php echo date('h:ia',strtotime($latestMember[0]->c_date));?></p>
				</div>
				<div class="top_best_sub">
					<div class="heading">
						<span class="head_large_span">Latest Member</span>
					</div>
					
					<?php
						$userImage =  base_url().'images/user.png';
						if(isset($latestMember[0]->pic_path) && !empty($latestMember[0]->pic_path)) {
							$userImage = base_url().'uploads/'.$latestMember[0]->pic_path;
						}
					?>
					<div class="personality">
					<input type="hidden" name="user_id" value="<?php echo $encryptObj->get_encrypted_code($latestMember[0]->id); ?>">
						<a style="text-decoration: none;" href="<?php echo base_url().'profile?email='.$latestMember[0]->u_email;?>">
							<img src="<?php echo $userImage; ?>" height="100" width="70" />
						</a>
						<div class="personality_info">
							<a style="text-decoration: none;" href="<?php echo base_url().'profile?email='.$latestMember[0]->u_email;?>">
								<span class="plarge_span"><?php echo str_truncate_words($latestMember[0]->f_name.' '.$latestMember[0]->l_name, 14); ?></span>
							</a>
							<span class="psmall_span"><?php echo $latestMember[0]->current_location_1; ?> &nbsp; <?php echo date('m/d/Y',strtotime($latestMember[0]->birthday));?></span>
							<div class="progressbar">
								<!--Violet-->
								<div class="bottom_bar"></div>
								<div class="i_bar_v"
									style="border-right: 1px solid #111; width: 0%;">
									<span class="inside-middle"></span>
								</div>
								<div class="top_inf">
									<span class="span_name">Rank</span>
									<span class="span_score">
										<span class="span_score"><?php echo $latestMember[0]->rank; ?></span>
									</span>
								</div>
							</div>
							<div class="progressbar">
								<!--green-->
								<div class="bottom_bar"></div>
								<div class="i_bar_g" style="border-right: 1px solid #111; width: 0%;">
									<span class="inside-middle"></span>
								</div>
								<div class="top_inf">
									<span class="span_name">Popularity</span>
									<span class="span_score">
										<span class="span_score"><?php echo $latestMember[0]->popularity; ?></span> 
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="pe_button_container" style="width: 260px; margin-top: 7px;">
						<button type="button" class="pe_btn" style="margin-left: 20px;">
							<span>Add Favourites</span>
						</button>
						<button type="button" class="pe_btn">
							<span>New Gossip</span>
						</button>
					</div>					
				</div>
				<?php endif;?>
			</div>
			
			<div class="highest_traited">				
			</div>
			
			<div class="member_spotlight">

				<div class="top_best_sub" style="margin-left: 45px;">
					<div class="heading">
						<span class="head_large_span">Member Spotlight</span>
					</div>
					<div class="personality">
						<img src="<?php echo base_url(); ?>images/steve_buscemi.png"
							height="100" width="70" />
						<div class="personality_info">
							<span class="plarge_span">Steve Buscemi</span> <span
								class="psmall_span">New York &nbsp; 1/15/11</span>
							<div class="progressbar">
								<!--Violet-->
								<div class="bottom_bar"></div>
								<div class="i_bar_v" style="border-right: 1px solid #111;">
									<span class="inside-middle"></span>
								</div>
								<div class="top_inf">
									<span class="span_name">Rank</span><span class="span_score"><span
										class="span_score">1875</span> </span>
								</div>
							</div>
							<div class="progressbar">
								<!--Orange-->
								<div class="bottom_bar"></div>
								<div class="i_bar_o" style="border-right: 1px solid #111;">
									<span class="inside-middle"></span>
								</div>
								<div class="top_inf">
									<span class="span_name">Popularity</span><span
										class="span_score"><span class="span_score">20</span> </span>
								</div>
							</div>
						</div>
					</div>
					<div class="pe_button_container"
						style="width: 260px; margin-top: 7px;">
						<button class="pe_btn" style="margin-left: 20px;">
							<span>Add Favourites</span>
						</button>
						<button class="pe_btn">
							<span>New Gossip</span>
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="bottom_container">
			<div class="heading_large">
				<span class="head_large_span">Latest Gossip About Member</span>
			</div>
			<div class="gossip_wrap">
				<div class="gossiper_container" style="width: 275px;">
					<div class="spec_circle_sky" style="margin: 50px 0 0 -10px;">
						<p class="p_sky">3</p>
					</div>
					<div class="gossiper">
						<img src="<?php echo base_url(); ?>images/neil_p_harris.png"
							height="100" width="70" />
						<div class="personality_info">
							<span class="plarge_span">Neil P. Harris</span><br> <span
								class="psmall_span">New York &nbsp; 1/15/11</span>
							<div class="progressbar">
								<!--Green-->
								<div class="bottom_bar"></div>
								<div class="i_bar_g"
									style="border-right: 1px solid #111; width: 90%">
									<span class="inside-middle"></span>
								</div>
								<div class="top_inf">
									<span class="span_name">Hardworking</span><span
										class="span_score"><span class="span_score">75</span> </span>
								</div>
							</div>
							<div class="progressbar">
								<!--violet-->
								<div class="bottom_bar"></div>
								<div class="i_bar_v"
									style="border-right: 1px solid #111; width: 75%">
									<span class="inside-middle"></span>
								</div>
								<div class="top_inf">
									<span class="span_name">Rank</span><span class="span_score"><span
										class="span_score">1875</span> </span>
								</div>
							</div>
						</div>
					</div>
					<div class="pe_button_container"
						style="width: 260px; margin: 7px 0 0 20px; float: left;">
						<button class="pe_btn" style="margin-left: 20px;">
							<span>Add Favourites</span>
						</button>
						<button class="pe_btn">
							<span>New Gossip</span>
						</button>
					</div>
				</div>
				<div class="gossiper_container" style="width: 395px;">
					<div class="gossip_topic">
						<div style="width: 335px; height: 125px; float: left;">
							<div class="gossip_thread">
								<span class="head_large_span">Gossip Thread:</span> <input
									type="text" />
								<textarea></textarea>
							</div>
						</div>
						<div style="width: 25px; height: 125px; float: left;">
							<div id="like_div" style="cursor: pointer;">
								<img src="<?php echo base_url(); ?>images/tup.png"
									style="margin-top: 2px;">
							</div>
							<div class="progressbar_h">
								<div class="bottom_bar_h"></div>
								<div id="like_val" class="i_bar_h"
									style="border-right: 2px solid #111; background: #00A650;">
									<span class="inside-middle_h"></span>
								</div>
								<div class="top_info_h"></div>
							</div>
							<div id="dislike_div" style="cursor: pointer;">
								<img src="<?php echo base_url(); ?>images/tdown.png">
							</div>
						</div>
					</div>
					<div class="goss_topic_btn_container">
						<div class="goss_tell_replies btn_reorder"
							style="margin-left: 12px;">
							<span class="goss_span">Gossip replies:</span><span
								class="goss_span">2</span>
						</div>
						<div class="goss_tell_btn btn_reorder" style="margin-left: 95px;">
							<span><a href="">View all gossip</a> </span>
						</div>
						<div class="goss_tell_btn btn_reorder" style="margin-left: 10px;">
							<span><a class="dummy_reply_btn">Reply to all gossip</a> </span>
						</div>
					</div>
				</div>
				<div class="gossiper_container" style="width: 250px;">
					<div class="gossiper">
						<img src="<?php echo base_url(); ?>images/boy_george.png"
							height="100" width="70" />
						<div class="gossiper_info">
							<span class="plarge_span">Boy George</span> <span
								class="psmall_span">New York &nbsp; 1/15/11</span>
							<div class="progressbar">
								<!--Green-->
								<div class="bottom_bar"></div>
								<div class="i_bar_g"
									style="border-right: 1px solid #111; width: 90%">
									<span class="inside-middle"></span>
								</div>
								<div class="top_inf">
									<span class="span_name">Hardworking</span><span
										class="span_score"><span class="span_score">75</span> </span>
								</div>
							</div>
							<div class="progressbar">
								<!--violet-->
								<div class="bottom_bar"></div>
								<div class="i_bar_v"
									style="border-right: 1px solid #111; width: 75%">
									<span class="inside-middle"></span>
								</div>
								<div class="top_inf">
									<span class="span_name">Rank</span><span class="span_score"><span
										class="span_score">1875</span> </span>
								</div>
							</div>
						</div>
					</div>
					<div class="pe_button_container"
						style="width: 260px; margin-top: 7px; float: left;">
						<button class="pe_btn" style="margin-left: 20px;">
							<span>Add Favourites</span>
						</button>
						<button class="pe_btn">
							<span>New Gossip</span>
						</button>
					</div>
				</div>
			</div>
			<div
				style="width: 940px; height: 70px; padding: 10px; text-align: center;">
				<div style="float: left; margin: 0 0 0 185px;">
					<p
						style="font-style: italic; font-weight: bold; color: #000; font-size: 16px;">Didn't
						find someone you were looking for?</p>
				</div>
				<div class="leave_gossip">
					<a href="#" style="text-decoration: none; color: #2E3192;">
						<p style="margin: 5px 0 0 0;">Leave gossip and invite them to join</p>
					</a>		
				</div>
				
			</div>
		</div>
		<form action="#" name="search_query_form" id="search_query_form" style="display: none;">
			<?php if(!empty($searchQuery)): ?>
			
			<?php endif;?>
		</form>
	</div>
	
	<script type="text/javascript">
		$(function(){

			$('a.selectTrait').on('click',function(e){
				e.preventDefault();
				$this = $(this);
				var traitName = $this.text(),
					traitValue = $this.attr('href');
				//img = $('#selectedView').find('img').html();
				$('#selectedView').text(traitName);
				$('input[name=sub_trait]').val(traitValue);
			});
			
			$('form#group_form').on('submit',function(e){
				e.preventDefault();
				$this = $(this);
				$formData = $this.serializeArray();
				$('.middle_left').find('input[name=user_id]').each(function(i){
					user = { name : "user_id[]", value : $(this).val() };
					$formData.push(user);
				});
				$.ajax({
					url : BASE+'search/get_memberinfo_by_trait',
					type: 'POST',
					data : $formData,
					success : function(replyData){
						$('.middle_left').fadeOut('slow',function(){
							$(this).html("").html(replyData).fadeIn();
						});
					}
				});
				getHighestTraitUserByTraitId();
			});

			$('form#group_person_select_form').on('submit',function(e){
				e.preventDefault();
				$this = $(this);
				$formData = $this.serializeArray();
				
				if($formData[0].value != "" ) {
					$('.middle_left').find('input[name=user_id]').each(function(i){
						user = { name : "user_id[]", value : $(this).val() };
						$formData.push(user);
					});
					$.ajax({
						url : BASE+'search/get_memberinfo_by_searchname',
						type: 'POST',
						data : $formData,
						success : function(replyData) {
							$('#searched_user').fadeOut('slow',function(){
								$(this).html("").html(replyData).fadeIn();
							});
						}
					});
				}
			});

			getHighestTraitUserByTraitId = function(){
		    	var formData = $('form#group_form').serializeArray();
		    	
		    	$('.middle_left').find('input[name=user_id]').each(function(i){
					user = { name : "user_id[]", value : $(this).val() };
					formData.push(user);
				});
		        $.ajax({
		            url : BASE+'search/get_group_highest_traituser/',
		            type : 'POST',
		            data : formData,
		            success : function(reply) {		                
	                    $('.highest_traited').fadeOut().html("");
	                    $('.highest_traited').html(reply).fadeIn();
		            }
		        });
		    };
		    getHighestTraitUserByTraitId();
		    

			$('a#add_to_favourite').on('click',function(e) {
				e.preventDefault();
				$('#create_group_popup').fadeIn('slow');
			});

			$('button.quote_cancel').on('click',function(){
				$this = $(this);
				$container = $this.parents('.bubble_container');
				$container.fadeOut('slow',function(){
					$container.find('input[type=text]').val("");
				});
			});

			$('#create_group_popup').on('click','.quote_save',function(){				
				$this = $(this);
				$container = $this.parents('.bubble_container');
				group_name = $container.find('input[name=group_name]').val();
				if(group_name.length > 0){
					$.ajax({
						url : BASE+'/search/create_group',
						type: 'POST',
						data : { group_name : group_name },
						success : function(replyData) {
							if(replyData==1){
								load_notification_bar("Group created");
					            delay_and_hide_notification_bar(3000);
					            window.location.href = BASE+'group/my_group?gname='+group_name;
								$("#add_to_favourite").text("Edit Group");
								$("#add_to_favourite").attr('id','edit_group');
								$container.fadeOut('slow',function(){
									$container.find('input[type=text]').val("");
								});
							}else {
								alert(replyData);
							}
						}
					});
				}else {
					alert("Please insert group name");
				}
			});

			getGraphDayDataList = function (){
				//var graphData;
		    	var formData = $('form#group_form').serializeArray();
		        $.ajax({
		            url : BASE+'search/get_graphday_datalist/',
		            type : 'POST',
		            data : formData,
		            success : function(reply) {
		                var replyData = $.parseJSON(reply);

		                if(replyData.reply==1){
		                	graphData = replyData.graphData;
		                	//console.log(graphData[0].percentage);
		                	drawDayCanvas(graphData[0].percentage);
		                }else {
		                	
			            }
		            }
		        });
		        
		    };
			
			getGraphDayDataList();
			
		    getGraphYearDataList = function (){
				//var graphData;
		    	var formData = $('form#group_form').serializeArray();
		        $.ajax({
		            url : BASE+'search/get_graphyear_datalist/',
		            type : 'POST',
		            data : formData,
		            success : function(reply) {
		                var replyData = $.parseJSON(reply);

		                if(replyData.reply==1){
		                	graphData = replyData.graphData;			                	
		                	drawYearCanvas(graphData[0].frequency);
		                }else {
		                	
			            }
		            }
		        });
		        
		    };
			
		    getGraphYearDataList();

			
			function drawDayCanvas(graphFrequencyDay){
				graphFrequencyDayData = graphFrequencyDay.split(",");
				var c = document.getElementById("g1");

			   	if(c){
					var ctx=c.getContext("2d");
					ctx.beginPath();
					ctx.moveTo(0,190);
					var y;
					//console.log(graphFrequencyDayData.length);
					for(var i=0;i<graphFrequencyDayData.length;i++){
						y = Math.floor((parseInt(graphFrequencyDayData[i])*190)/100);
						//console.log(parseInt(graphFrequencyDayData[i]));
						y = 190-y;
						ctx.lineTo(i*1.85,y);
					}
					
					//console.log(a);
					ctx.lineTo(370,190);
					
					// add linear gradient
					var grd = ctx.createLinearGradient(370, 0, 370, 190);
					// light blue
					grd.addColorStop(0, '#8ED6FF');   
					// dark blue
					grd.addColorStop(1, '#004CB3');
					ctx.fillStyle = grd;
					
					ctx.lineJoin = 'round';
					ctx.lineWidth = 1;
					//ctx.fillStyle = '#3972C1';
					    
					ctx.shadowColor = '#999';
					ctx.shadowBlur = 0;
					ctx.shadowOffsetX = 2;
					ctx.shadowOffsetY = 2;
					ctx.fill();
				    
				    
				    
				    var x = 18.5;
				    
				    for(var j = 0 ; j<20; j++){
					    ctx.beginPath();
					    ctx.moveTo(x,0);
					    ctx.lineTo(x,190);
					    ctx.shadowBlur = 0; 
					    ctx.shadowColor= '#fff'; 
					    ctx.strokeStyle = '#000000';
					    ctx.shadowColor = '#333';
					    ctx.shadowBlur = 0;
					    ctx.shadowOffsetX = 0;
					    ctx.shadowOffsetY = 0;
					    if(j==8){
						    x +=17.5;
						    ctx.lineWidth = 1;
						    ctx.stroke();
				    	}
					    else if(j==9){
						    x +=18.5;
						    ctx.lineWidth = 3;
						    ctx.stroke();
					    }
					    else{
						    x +=18.5;
						    ctx.lineWidth = 1;
						    ctx.stroke();
					    }
				    
				    }
			    
				}
			}

			function drawYearCanvas(graphFrequencyData1){
				graphFrequencyData = graphFrequencyData1.split(",");
				var c=document.getElementById("g2");
				var arr1 = new Array();
				var arr2 = new Array();
			   	if(c){
					var ctx=c.getContext("2d");
				
					//////////
					ctx.beginPath();
					ctx.moveTo(0,95);
					var y;
			
					for(var i=0; i< graphFrequencyData.length;){
						if(graphFrequencyData[i] >= 0){
							y = Math.floor((graphFrequencyData[i]*95)/100);
							arr1.push(y);
							y = (95-y);
							ctx.lineTo(i,y);
							i = i+1;
						}else {
							y = Math.floor(null*95);
							arr1.push(y);
							y = (95-y);
							//ctx.lineTo(i,y);
							i = i+1;
						}
					}
				
					ctx.lineTo(370,95);
				
					// add linear gradient
			    	var grd = ctx.createLinearGradient(370, 0, 370, 95);
			      	// light blue
			      	grd.addColorStop(0, '#8ED6FF');   
			      	// dark blue
			      	grd.addColorStop(1, '#004CB3');
			      	ctx.fillStyle = grd;
				
					ctx.lineJoin = 'round';
					ctx.lineWidth = 1;
				    //ctx.fillStyle = '#3972C1';
				    ctx.shadowColor = '#999';
				    ctx.shadowBlur = 0;
				    ctx.shadowOffsetX = 2;
				    ctx.shadowOffsetY = 2;
				    ctx.fill();
				    
				    /////////////////
				    ctx.beginPath();
					ctx.moveTo(0,95);
					var y;
			
					for(var i=0;i<graphFrequencyData.length;){
						if(graphFrequencyData[i] <= 0){
							y = Math.floor((Math.abs(graphFrequencyData[i])*95)/100);
							
							arr2.push(y);
							y = 95+y;
							ctx.lineTo(i,y);
							i = i+1;
						}else {
							y = Math.floor(null*95);
							arr2.push(y);
							y = (95-y);
							//ctx.lineTo(i,y);
							i = i+1;
						}
					}
				
					ctx.lineTo(370,95);
				
					// add linear gradient
			      	var grd = ctx.createLinearGradient(370, 95, 370, 190);
			      	// light blue
			      	grd.addColorStop(1, '#8ED6FF');   
			      	// dark blue
			      	grd.addColorStop(0, '#004CB3');
			      	ctx.fillStyle = grd;
				
					ctx.lineJoin = 'round';
					ctx.lineWidth = 1;
				    //ctx.fillStyle = '#3972C1';
				    ctx.shadowColor = '#999';
				    ctx.shadowBlur = 0;
				    ctx.shadowOffsetX = 2;
				    ctx.shadowOffsetY = 2;
				    ctx.fill();
				    	    
				    ctx.beginPath();
					ctx.moveTo(0,95);
					
					//console.log(arr1.length);
					arr1.reverse();
					arr2.reverse();
				
				    for(var i=0; i< graphFrequencyData.length;){
					    var p1 = arr1.pop();
					    var p2 = arr2.pop();
					    var yy = p1 - p2;
					    if(yy < 0){
					    	yy =  (95 + Math.abs(yy));
					    }
					    else{
					    	yy =  95-yy;
					    }
					    
					   //console.log(p1+':'+p2+':'+yy);
					    //ctx.lineTo(i,yy);
					    i = i+10;
				    }
				    ctx.lineTo(370,95);
					ctx.lineWidth = 2;
					ctx.strokeStyle = '#222';
					ctx.shadowColor = '#222';
				    ctx.shadowBlur = 0;
				    ctx.shadowOffsetX = 0;
				    ctx.shadowOffsetY = 0;
				    ctx.stroke();
					
					/////////////
					ctx.beginPath();
					ctx.moveTo(0,95);
					ctx.lineTo(370,95);
					ctx.lineWidth = 2;
					ctx.strokeStyle = '#000000';
					ctx.shadowColor = '#333';
					ctx.shadowBlur = 0;
				    ctx.shadowOffsetX = 0;
				    ctx.shadowOffsetY = 0;
				    ctx.stroke();
				    
				    
				    ///////////
				    var x = 30.83;
				    
				    for(var j = 0 ; j<20; j++){
					    ctx.beginPath();
					    ctx.moveTo(x,0);
					    ctx.lineTo(x,190);
					    ctx.shadowBlur = 0; 
					    ctx.shadowColor= '#fff'; 
					    ctx.strokeStyle = '#000000';
					    ctx.shadowColor = '#333';
					    ctx.shadowBlur = 0;
					    ctx.shadowOffsetX = 0;
					    ctx.shadowOffsetY = 0;
					    x +=30.83;
					    ctx.lineWidth = 1;
					    ctx.stroke();
				    }
			    
				}
			
		    }
		});
	</script>
	<!--</div>-->
	<!--Footer>-->
	<?php include 'footer.php' ?>
	<script type="text/javascript">
	set_postion_fixed();
    initialize_notification_bar("42%","31%");
	</script>