<!-- 
<pre>
	<?php //print_r($search_data); die();?>
</pre>
-->

<div class="as_sr" style="margin-left: 40px;">
    <span style="margin:0;">Search Results</span><span>(<?php echo sizeof($search_data); ?>)</span> 
</div>
<div id="search_user_list" class="as_middle_s">
	<form action="#" name="form_search_user" id="form_search_user">
    <?php
    $z=1;
    if (sizeof($search_data)>0){
        foreach($search_data as $rowData){ 
			$w = 0;
			$avgPoint = $rowData->sub_category_avg_point;
			$w = abs($avgPoint).'%';
			if($avgPoint < 0) {
				$cname = 'i_bar_red';
			}else {
				$cname = $rowData->color_class;
			}
	?>
        <div class="as_sr_container" id="li_<?php echo $z.$rowData->id.$rowData->sub_category_value;?>">
        	<input class="hidden_user_id" type="hidden" name="user_id[]" value="<?php echo $encryptObj->get_encrypted_code($rowData->id);?>">
            <div class="pe_circle_purple">
                    <p class="p_multi"><?php echo $z; ?></p>
            </div>
            <div class="as_sr_progress">
            <?php if($rowData->pic_path!=""){ ?>
            	<img src="<?php echo base_url().'uploads/'.$rowData->pic_path; ?>" height="45" width="40">
            <?php }else{ ?>
            	<img src="<?php echo base_url().'images/user.png'; ?>" height="45" width="40">
            <?php } ?>
            	<span class="as_l_span">
                    <a href="<?php echo base_url()."profile?email=".$rowData->u_email; ?>"><?php echo str_truncate_words($rowData->f_name." ". $rowData->l_name, 13);?></a>
                </span>
            	<span class="as_s_span"><?php echo $rowData->current_location_1;?></span>
            	<br>
            	<span class="as_s_para"><?php echo date('m/d/Y',strtotime($rowData->birthday));?></span>
                <div class="progressbar" style="float:right;margin:5px 5px 0 0px;"><!--green-->
                	<div class="bottom_bar"></div>
                    <div class="<?php echo $cname;?>" style="border-right:1px solid #111;width:<?php echo $w;?>;">
                    	<span class="inside-middle"></span>
                    </div>
                    <div class="top_inf"><span class="span_name"><?php echo $rowData->sub_category_value; ?></span><span class="span_score"><?php echo $avgPoint; ?></span></div>
                </div>
            </div>
            <div class="as_circle_plus user_select">
                  <input style="display:none;" name="selected_user_id" type="checkbox" value="<?php echo $encryptObj->get_encrypted_code($rowData->id);?>" />  
            </div>
        </div>
    <?php
       
        $z++;
        } 
    }else{ ?>
          <div class="as_sr">
                <span style="margin:0;">  No Result Found</span> 
           </div>
        
   <?php } ?>
   </form>
</div>