<?php if(!empty($userTraitInfo)): ?>
<?php foreach ($userTraitInfo as $key=>$userData):?>
<?php 
	$userImage =  base_url().'images/user.png';
	if(isset($userData->pic_path) && !empty($userData->pic_path)) {
		$userImage = base_url().'uploads/'.$userData->pic_path;
	}
?>
<div class="pe_container">
	<input type="hidden" name="user_id" value="<?php echo $encryptObj->get_encrypted_code($userData->u_id); ?>">
	<div class="pe_circle_purple">
		<p class="p_multi">
			<?php echo $key+1; ?>
		</p>
	</div>
	<div class="progress_extended">
		<a style="text-decoration: none;" href="<?php echo base_url().'profile?email='.$userData->u_email;?>">
			<img src="<?php echo $userImage; ?>" height="50" width="50" />
		</a>
		<div class="pe_details">
			<a style="text-decoration: none;" href="<?php echo base_url().'profile?email='.$userData->u_email;?>">
				<span class="pe_span_large"><?php echo str_truncate_words($userData->f_name.' '.$userData->l_name, 20);?></span>
			</a>
			<span class="pe_span_small"><?php echo $userData->current_location_1; ?>
			</span> <span class="pe_span_small"><?php echo date('m/d/Y',strtotime($userData->birthday));?>
			</span>
		</div>
		<div class="pe_progress_container">
			<div class="progressbar" style="float: right; margin: 5px 5px 0 0px;">
				<!--Violet-->
				<div class="bottom_bar"></div>
				<div class="i_bar_v"
					style="border-right: 1px solid #111; width: 0%;">
					<span class="inside-middle"></span>
				</div>
				<div class="top_inf">
					<span class="span_name">Rank</span><span class="span_score"><span
						class="span_score"><?php echo $userData->rank; ?> </span> </span>
				</div>
			</div>
		<?php
			$w = 0;
			$avgPoint = $userData->sub_category_avg_point;
			$w = abs($avgPoint).'%';
			if($avgPoint < 0) {
				$cname = 'i_bar_red';
			}else {
				$cname = 'i_bar_g';
			}
		?>
			<div class="progressbar" style="float: right; margin: 5px 5px 0 0px;">
				<!--green-->
				<div class="bottom_bar"></div>
				<div class="<?php echo $cname; ?>" style="border-right: 1px solid #111; width: <?php echo $w; ?>;">
					<span class="inside-middle"></span>
				</div>
				<div class="top_inf">
					<span class="span_name"><?php echo $userData->sub_category_value; ?>
					</span> <span class="span_score"><span class="span_score"><?php echo $avgPoint; ?>
					</span> </span>
				</div>
			</div>
		</div>
		<div class="pe_button_container">
			<button class="pe_btn" style="margin-left: 40px;">
				<span>Add Favourites</span>
			</button>
			<button class="pe_btn">
				<span>New Gossip</span>
			</button>
		</div>
	</div>
</div>
<?php endforeach;?>
<?php endif;?>